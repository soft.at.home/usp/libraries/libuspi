# USP IMTP management library

[[_TOC_]]

## Introduction

The goal of this library is to group up common functions that are needed to maintain a USP IMTP connection between several processes. The functionality can be split up into 3 parts:

- Maintaining the connection, reading and writing data over the connection
- Handling USP messages in a generic way
- Extending and shrinking the Discovery data model with common API functions

## Building, installing and testing

### Docker container

You could install all tools needed for testing and developing on your local machine, but it is easier to just use a pre-configured environment. Such an environment is already prepared for you as a docker container.

1. Install docker

    Docker must be installed on your system.

    If you have no clue how to do this here are some links that could help you:

    - [Get Docker Engine - Community for Ubuntu](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
    - [Get Docker Engine - Community for Debian](https://docs.docker.com/install/linux/docker-ce/debian/)
    - [Get Docker Engine - Community for Fedora](https://docs.docker.com/install/linux/docker-ce/fedora/)
    - [Get Docker Engine - Community for CentOS](https://docs.docker.com/install/linux/docker-ce/centos/)  <br /><br />
    
    Make sure you user id is added to the docker group:

    ```
    sudo usermod -aG docker $USER
    ```

1. Fetch the container image

    To get access to the pre-configured environment, all you need to do is pull the image and launch a container.

    Pull the image:

    ```bash
    docker pull registry.gitlab.com/soft.at.home/docker/oss-dbg:latest
    ```

    Before launching the container, you should create a directory which will be shared between your local machine and the container.

    ```bash
    mkdir -p ~/workspace/amx/usp/libraries/
    ```

    Launch the container:

    ```bash
    docker run -ti -d --name oss-dbg --restart always --cap-add=SYS_PTRACE --sysctl net.ipv6.conf.all.disable_ipv6=1 -e "USER=$USER" -e "UID=$(id -u)" -e "GID=$(id -g)" -v ~/workspace/:/home/$USER/workspace/ registry.gitlab.com/soft.at.home/docker/oss-dbg:latest
    ```

    The `-v` option bind mounts the local directory for development in the container, at the exact same place.
    The `-e` options create environment variables in the container. These variables are used to create a user name with exactly the same user id and group id in the container as on your local host (user mapping).

    You can open as many terminals/consoles as you like:

    ```bash
    docker exec -ti --user $USER oss-dbg /bin/bash
    ```

### Building

#### Prerequisites

- [libamxc](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxc)
- [libamxp](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxp)
- [libamxd](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxd)
- [libamxb](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxb)
- [libsahtrace](https://gitlab.com/soft.at.home/network/libsahtrace)
- [libimtp](https://gitlab.com/soft.at.home/usp/libraries/libimtp)
- [libusp](https://gitlab.com/soft.at.home/usp/libraries/libusp)

You can install these libraries from source or using their debian packages. To install them from source, refer to their corresponding repositories for more information.
To install them using debian packages, you can run

```bash
sudo apt update
sudo apt install -y sah-lib-sahtrace-dev libamxc libamxp libamxd libamxb libimtp libusp
```

#### Build libuspi

1. Clone the git repository

    To be able to build it, you need the source code. So open the directory just created for development and clone this library in it.

    ```bash
    cd ~/workspace/amx/usp/libraries/
    git clone git@gitlab.com:prpl-foundation/components/core/libraries/libuspi.git
    ``` 

1. Build it

    ```bash
    cd ~/workspace/amx/usp/libraries/libuspi
    make
    ```

### Installing

#### Using make target install

You can install your own compiled version easily in the container by running the install target.

```bash
cd ~/workspace/amx/usp/libraries/libuspi
sudo make install
```

#### Using package

From within the container you can create packages.

```bash
cd ~/workspace/amx/usp/libraries/libuspi
make package
```

The packages generated are:

```
~/workspace/amx/usp/libraries/libuspi/libuspi-<VERSION>.tar.gz
~/workspace/amx/usp/libraries/libuspi/libuspi-<VERSION>.deb
```

You can copy these packages and extract/install them.

For ubuntu or debian distributions use dpkg:

```bash
sudo dpkg -i ~/workspace/amx/usp/libraries/libuspi/libuspi-<VERSION>.deb
```

### Running

#### Run time prerequisites

- [mod-amxb-usp](https://gitlab.com/prpl-foundation/components/ambiorix/modules/amxb_backends/amxb_usp)

This library mainly uses amxb functions for setting up the IMTP USP connection, however it also uses `amxb_read_raw` to read raw data from the back-end connection. In the current implementation, it will only be possible to read raw data from a USP back-end connection and format the result to an `imtp_tlv_t`. Therefore, you need to use the USP back-end when using this library. It can be installed from source or from a debian package.

```bash
sudo apt install -y mod-amxb-usp
```

### Testing

#### Prerequisites

- [mod-amxb-usp](https://gitlab.com/prpl-foundation/components/ambiorix/modules/amxb_backends/amxb_usp)
- [libamxo](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxo)

#### Run tests

1. Install dependencies

    The USP back-end and libamxo are needed for testing this library. If you haven't done so already, you can install them from their debian packages

    ```bash
    sudo apt install mod-amxb-usp libamxo
    ```

1. Run tests

    You can run the tests by executing the following command.
    
    ```bash
    cd ~/workspace/amx/usp/libraries/libuspi/test
    make
    ```
    
    Or this command if you also want the coverage tests to run:
    
    ```bash
    cd ~/workspace/amx/usp/libraries/libuspi/test
    make run coverage
    ```
    
    Or from the root directory of this repository,
    
    ```bash
    cd ~/workspace/amx/usp/libraries/libuspi
    make test
    ```
    
    This last will run the unit-tests and generate the test coverage reports in one go.

#### Coverage reports

The coverage target will generate coverage reports using [gcov](https://gcc.gnu.org/onlinedocs/gcc/Gcov.html) and [gcovr](https://gcovr.com/en/stable/guide.html).

A summary for each file (*.c file) is printed in your console after the tests are run.
A HTML version of the coverage reports is also generated. These reports are available in the output directory of the compiler used.
Example: using native gcc
When the output of `gcc -dumpmachine` is `x86_64-linux-gnu`, the HTML coverage reports can be found at `~/workspace/amx/usp/libraries/libuspi/output/x86_64-linux-gnu/coverage/report.`

You can easily access the reports in your browser.
In the container start a python3 http server in background.

```bash
cd ~/workspace/
python3 -m http.server 8080 &
```

Use the following url to access the reports `http://<IP ADDRESS OF YOUR CONTAINER>:8080/libraries/libuspi/output/<MACHINE>/coverage/report`
You can find the ip address of your container by using the `ip` command in the container.

Example:

```bash
USER@<CID>:~/libraries/libuspi$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
173: eth0@if174: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default 
    link/ether 02:42:ac:11:00:07 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet 172.17.0.7/16 scope global eth0
       valid_lft forever preferred_lft forever
    inet6 2001:db8:1::242:ac11:7/64 scope global nodad 
       valid_lft forever preferred_lft forever
    inet6 fe80::42:acff:fe11:7/64 scope link 
       valid_lft forever preferred_lft forever
```

In this case the IP address of the container is `172.17.0.7`.
So the uri you should use is: `http://172.17.0.7:8080/amx/usp/libraries/libuspi/output/x86_64-linux-gnu/coverage/report/`
