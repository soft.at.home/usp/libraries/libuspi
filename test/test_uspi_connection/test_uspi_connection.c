/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxb/amxb.h>
#include <amxb/amxb_register.h>
#include <amxo/amxo.h>

#include "uspi.h"
#include "uspi/uspi_connection.h"

#include "test_uspi_connection.h"
#include "mock.h"
#include "common.h"

static const char* listen_uri = "usp:/tmp/test_uspi_listen.sock";
static int counter = 0;

static void test_setup_connection(uspi_con_t** con_listen,
                                  uspi_con_t** con_accept,
                                  uspi_con_t** con_connect) {
    assert_int_equal(uspi_con_listen(con_listen, listen_uri), 0);
    assert_int_equal(uspi_con_connect(con_connect, listen_uri), 0);
    assert_int_equal(uspi_con_accept(*con_listen, con_accept), 0);
}

static void signal_received(const char* const sig_name,
                            UNUSED const amxc_var_t* const data,
                            UNUSED void* const priv) {
    printf("Signal %s received", sig_name);
    counter++;
}

int test_setup(UNUSED void** state) {
    assert_int_equal(amxb_be_load("/usr/bin/mods/usp/mod-amxb-usp.so"), 0);

    return 0;
}

int test_teardown(UNUSED void** state) {
    remove(listen_uri);
    assert_int_equal(amxb_be_remove("usp"), 0);

    return 0;
}

void test_can_setup_listen_socket(UNUSED void** state) {
    uspi_con_t* con = NULL;

    // Generic failures
    assert_int_equal(uspi_con_listen(NULL, NULL), -1);
    assert_int_equal(uspi_con_listen(&con, NULL), -1);
    assert_int_equal(uspi_con_listen(&con, ""), -1);
    uspi_con_disconnect(NULL);
    uspi_con_disconnect(&con);

    assert_int_equal(uspi_con_listen(&con, listen_uri), 0);

    uspi_con_disconnect(&con);
}

void test_can_connect_and_accept(UNUSED void** state) {
    uspi_con_t* con_listen = NULL;
    uspi_con_t* con_connect = NULL;
    uspi_con_t* con_accept = NULL;

    // Generic failures
    assert_int_equal(uspi_con_connect(NULL, NULL), -1);
    assert_int_equal(uspi_con_connect(&con_connect, NULL), -1);
    assert_int_equal(uspi_con_connect(&con_connect, ""), -1);
    assert_int_equal(uspi_con_accept(NULL, NULL), -1);
    assert_int_equal(uspi_con_get_fd(NULL), -1);

    test_setup_connection(&con_listen, &con_accept, &con_connect);
    assert_int_equal(uspi_con_accept(con_listen, NULL), -1);

    assert_in_range(uspi_con_get_fd(con_listen), 3, UINT32_MAX);
    assert_in_range(uspi_con_get_fd(con_connect), 3, UINT32_MAX);
    assert_in_range(uspi_con_get_fd(con_accept), 3, UINT32_MAX);
    printf("Listen connection fd = %d\n", uspi_con_get_fd(con_listen));
    printf("Connect connection fd = %d\n", uspi_con_get_fd(con_connect));
    printf("Accept connection fd = %d\n", uspi_con_get_fd(con_accept));

    uspi_con_disconnect(&con_connect);
    uspi_con_disconnect(&con_listen);
}

void test_can_read_from_con(UNUSED void** state) {
    uspi_con_t* con_listen = NULL;
    uspi_con_t* con_connect = NULL;
    uspi_con_t* con_accept = NULL;
    imtp_tlv_t* tlv = NULL;
    imtp_tlv_t* tlv_dummy = NULL;

    create_dummy_tlv(&tlv_dummy);
    test_setup_connection(&con_listen, &con_accept, &con_connect);

    assert_int_equal(uspi_con_read(NULL, NULL), -1);
    assert_int_equal(uspi_con_read(con_connect, NULL), -1);

    will_return(__wrap_amxb_read_raw, 1);
    will_return(__wrap_amxb_read_raw, 5);
    will_return(__wrap_amxb_read_raw, 2);
    will_return(__wrap_amxb_read_raw, tlv_dummy->length);
    assert_int_equal(uspi_con_read(con_connect, &tlv), 0);
    assert_non_null(tlv);

    imtp_tlv_delete(&tlv_dummy);
    imtp_tlv_delete(&tlv);
    uspi_con_disconnect(&con_connect);
    uspi_con_disconnect(&con_listen);
}

void test_can_write_to_con(UNUSED void** state) {
    uspi_con_t* con_listen = NULL;
    uspi_con_t* con_connect = NULL;
    uspi_con_t* con_accept = NULL;
    imtp_tlv_t* tlv_dummy = NULL;

    create_dummy_tlv(&tlv_dummy);
    test_setup_connection(&con_listen, &con_accept, &con_connect);

    assert_int_equal(uspi_con_write(NULL, NULL), -1);
    assert_int_equal(uspi_con_write(con_connect, NULL), -1);
    assert_int_equal(uspi_con_write(con_connect, tlv_dummy), 0);

    imtp_tlv_delete(&tlv_dummy);
    uspi_con_disconnect(&con_connect);
    uspi_con_disconnect(&con_listen);
}

void test_can_add_eid(UNUSED void** state) {
    uspi_con_t* con_listen = NULL;
    uspi_con_t* con_connect = NULL;
    uspi_con_t* con_accept = NULL;
    const char* eid = "dummy";
    uint32_t len = strlen(eid);
    imtp_tlv_t* tlv_eid = NULL;

    test_setup_connection(&con_listen, &con_accept, &con_connect);

    assert_int_equal(uspi_con_add_eid(NULL, NULL), -1);
    assert_int_equal(uspi_con_add_eid(con_connect, NULL), -1);

    imtp_tlv_new(&tlv_eid, imtp_tlv_type_eid, len, (char*) eid, 0, IMTP_TLV_COPY);

    assert_int_equal(uspi_con_add_eid(con_connect, tlv_eid), 0);
    assert_int_equal(uspi_con_add_eid(con_connect, tlv_eid), -1);
    handle_events();

    imtp_tlv_delete(&tlv_eid);
    uspi_con_disconnect(&con_connect);
    uspi_con_disconnect(&con_listen);
}

void test_can_send_eid(UNUSED void** state) {
    uspi_con_t* con_listen = NULL;
    uspi_con_t* con_connect = NULL;
    uspi_con_t* con_accept = NULL;
    const char* eid = "dummy";

    test_setup_connection(&con_listen, &con_accept, &con_connect);

    assert_int_equal(uspi_con_send_eid(NULL, NULL), -1);
    assert_int_equal(uspi_con_send_eid(con_connect, NULL), -1);
    assert_int_equal(uspi_con_send_eid(con_connect, ""), -1);
    assert_int_equal(uspi_con_send_eid(con_connect, eid), 0);

    uspi_con_disconnect(&con_connect);
    uspi_con_disconnect(&con_listen);
}

void test_adding_eid_emits_signal(UNUSED void** state) {
    uspi_con_t* con_listen = NULL;
    uspi_con_t* con_connect = NULL;
    uspi_con_t* con_accept = NULL;
    const char* eid = "dummy";
    uint32_t len = strlen(eid);
    imtp_tlv_t* tlv_eid = NULL;
    amxp_signal_t* sig = NULL;

    amxp_sigmngr_add_signal(NULL, USPI_SIG_EID);
    amxp_slot_connect(NULL, USPI_SIG_EID, NULL, signal_received, NULL);

    test_setup_connection(&con_listen, &con_accept, &con_connect);

    imtp_tlv_new(&tlv_eid, imtp_tlv_type_eid, len, (char*) eid, 0, IMTP_TLV_COPY);
    assert_int_equal(uspi_con_add_eid(con_connect, tlv_eid), 0);

    handle_events();
    assert_int_equal(counter, 1);

    sig = amxp_sigmngr_find_signal(NULL, USPI_SIG_EID);
    amxp_sigmngr_remove_signal(NULL, USPI_SIG_EID);
    amxp_signal_delete(&sig);

    imtp_tlv_delete(&tlv_eid);
    uspi_con_disconnect(&con_connect);
    uspi_con_disconnect(&con_listen);
}

void test_toggle_and_check_flags(UNUSED void** state) {
    uspi_con_t* con_listen = NULL;
    uspi_con_t* con_connect = NULL;
    uspi_con_t* con_accept = NULL;

    test_setup_connection(&con_listen, &con_accept, &con_connect);

    assert_false(uspi_con_flag_is_set(NULL, USPI_CON_LISTEN));
    assert_true(uspi_con_flag_is_set(con_listen, USPI_CON_LISTEN));
    assert_true(uspi_con_flag_is_set(con_connect, USPI_CON_CONNECT));
    assert_true(uspi_con_flag_is_set(con_accept, USPI_CON_ACCEPT));

    assert_int_equal(uspi_con_flag_toggle(NULL, USPI_CON_AGENT), -1);
    assert_int_equal(uspi_con_flag_toggle(con_listen, USPI_CON_AGENT), 0);
    assert_true(uspi_con_flag_is_set(con_listen, USPI_CON_AGENT));
    assert_false(uspi_con_flag_is_set(con_listen, USPI_CON_CONTROLLER));

    assert_int_equal(uspi_con_flag_toggle(con_accept, USPI_CON_CONTROLLER), 0);
    assert_true(uspi_con_flag_is_set(con_accept, USPI_CON_CONTROLLER));
    assert_false(uspi_con_flag_is_set(con_accept, USPI_CON_AGENT));

    uspi_con_disconnect(&con_connect);
    uspi_con_disconnect(&con_listen);
}
