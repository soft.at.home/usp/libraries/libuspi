/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <setjmp.h>
#include <cmocka.h>

#include <imtp/imtp_frame.h>

#include "common.h"

static int sfd;

void create_dummy_tlv(imtp_tlv_t** tlv) {
    imtp_tlv_t* tlv_head = NULL;
    imtp_tlv_t* tlv_pbuf = NULL;
    uint32_t len_pbuf = 100;
    void* pbuf = calloc(1, len_pbuf);
    uint32_t len_head = 0;

    imtp_tlv_new(&tlv_head, imtp_tlv_type_head, len_head, NULL, 0, IMTP_TLV_TAKE);
    imtp_tlv_new(&tlv_pbuf, imtp_tlv_type_protobuf_bytes, len_pbuf, pbuf, 0, IMTP_TLV_TAKE);
    imtp_tlv_add(tlv_head, tlv_pbuf);

    *tlv = tlv_head;
}

void create_dummy_frame(imtp_frame_t** frame) {
    imtp_tlv_t* tlv_1 = NULL;
    imtp_tlv_t* tlv_2 = NULL;
    imtp_tlv_t* tlv_3 = NULL;
    char* topic_1 = "Hello";
    char* topic_2 = "World";
    char bytes[4] = { 0, 1, 2, 3};
    uint32_t len_1 = strlen(topic_1);
    uint32_t len_2 = strlen(topic_2);
    uint32_t len_3 = 4;

    imtp_frame_new(frame);
    imtp_tlv_new(&tlv_1, imtp_tlv_type_topic, len_1, topic_1, 0, IMTP_TLV_COPY);
    imtp_tlv_new(&tlv_2, imtp_tlv_type_topic, len_2, topic_2, 0, IMTP_TLV_COPY);
    imtp_tlv_new(&tlv_3, imtp_tlv_type_protobuf_bytes, len_3, bytes, 0, IMTP_TLV_COPY);
    imtp_frame_tlv_add(*frame, tlv_1);
    imtp_frame_tlv_add(*frame, tlv_2);
    imtp_frame_tlv_add(*frame, tlv_3);
}

void handle_events(void) {
    while(amxp_signal_read() == 0) {
        printf(".");
    }
    printf("\n");
}

void connection_read(UNUSED int fd, UNUSED void* priv) {
    return;
}

bool test_verify_data(const amxc_var_t* data, const char* field, const char* value) {
    bool rv = false;
    char* field_value = NULL;
    amxc_var_t* field_data = GETP_ARG(data, field);

    printf("Verify table data: check field [%s] contains [%s]\n", field, value);
    fflush(stdout);
    assert_non_null(field_data);

    field_value = amxc_var_dyncast(cstring_t, field_data);
    assert_non_null(field_data);

    rv = (strcmp(field_value, value) == 0);

    free(field_value);
    return rv;
}

void capture_sigalrm(void) {
    sigset_t mask;

    sigemptyset(&mask);
    sigaddset(&mask, SIGALRM);

    sigprocmask(SIG_BLOCK, &mask, NULL);

    sfd = signalfd(-1, &mask, 0);
}

void read_sigalrm(void) {
    struct signalfd_siginfo fdsi;
    ssize_t s;

    s = read(sfd, &fdsi, sizeof(struct signalfd_siginfo));
    if(fdsi.ssi_signo == SIGALRM) {
        printf("Got SIGALRM\n");
        fflush(stdout);
    } else {
        printf("Read unexpected signal\n");
        fflush(stdout);
    }
}
