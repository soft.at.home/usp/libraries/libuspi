/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_path.h>
#include <amxb/amxb.h>
#include <amxb/amxb_register.h>
#include <amxo/amxo.h>

#include <amxut/amxut_util.h>
#include <amxut/amxut_verify.h>

#include "uspi.h"
#include "uspi/uspi_subscription.h"

#include "test_uspi_subscription.h"
#include "dummy_be.h"
#include "common.h"

static amxd_dm_t dm;
static amxb_bus_ctx_t* bus_ctx = NULL;
static amxo_parser_t parser;
static const char* phonebook_definition = "../common/phonebook_definition.odl";
static const char* localagent_definition = "../common/tr181-localagent_definition.odl";
static const char* localagent_defaults = "../common/tr181-localagent_defaults.odl";
static const char* from_id = "me";
static const char* to_id = "you";
static const char* subs_inst = "LocalAgent.Subscription.1.";
static const char* contr_inst = "LocalAgent.Controller.1.";

static amxc_var_t* extract_notification(uspi_sub_ref_t* sub_ref, amxc_var_t* data, int expected_rv) {
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t* notification = NULL;
    int rv = 0;

    amxc_var_dump(data, STDOUT_FILENO);

    uspl_tx_new(&usp_tx, from_id, to_id);
    rv = uspi_subscription_build_notify(data, sub_ref, usp_tx, false);
    assert_int_equal(rv, expected_rv);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    when_null(usp_rx, exit);

    amxc_var_new(&notification);
    if(uspl_notify_extract(usp_rx, notification) != 0) {
        amxc_var_delete(&notification);
        goto exit;
    }
    amxc_var_dump(notification, STDOUT_FILENO);

exit:
    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
    return notification;
}

static void build_obj_creation(UNUSED const char* const sig_name,
                               const amxc_var_t* const data,
                               void* const priv) {
    uspi_sub_ref_t* sub_ref = (uspi_sub_ref_t*) priv;
    amxc_var_t* notification = NULL;
    amxc_var_t copy_data;

    amxc_var_init(&copy_data);
    amxc_var_copy(&copy_data, data);
    notification = extract_notification(sub_ref, &copy_data, 0);

    assert_true(test_verify_data(notification, "obj_creation.keys.Alias", "contact-1"));
    assert_true(test_verify_data(notification, "subscription_id", "obj-creation"));

    amxc_var_clean(&copy_data);
    amxc_var_delete(&notification);
}

static void build_vc_object(UNUSED const char* const sig_name,
                            const amxc_var_t* const data,
                            void* const priv) {
    uspi_sub_ref_t* sub_ref = (uspi_sub_ref_t*) priv;
    amxc_var_t* notification = NULL;
    amxc_var_t copy_data;

    amxc_var_init(&copy_data);
    amxc_var_copy(&copy_data, data);
    notification = extract_notification(sub_ref, &copy_data, 1);

    assert_non_null(notification);
    assert_true(test_verify_data(notification, "value_change.param_path", "Phonebook.Contact.1.LastName"));
    assert_true(test_verify_data(notification, "value_change.param_value", "Bond"));
    assert_true(test_verify_data(notification, "subscription_id", "vc-object"));
    amxc_var_delete(&notification);

    notification = extract_notification(sub_ref, &copy_data, 0);
    assert_non_null(notification);
    assert_true(test_verify_data(notification, "value_change.param_path", "Phonebook.Contact.1.FirstName"));
    assert_true(test_verify_data(notification, "value_change.param_value", "James"));
    assert_true(test_verify_data(notification, "subscription_id", "vc-object"));

    amxc_var_clean(&copy_data);
    amxc_var_delete(&notification);
}

static void build_vc_single_param(UNUSED const char* const sig_name,
                                  const amxc_var_t* const data,
                                  void* const priv) {
    uspi_sub_ref_t* sub_ref = (uspi_sub_ref_t*) priv;
    amxc_var_t* notification = NULL;
    amxc_var_t copy_data;

    amxc_var_init(&copy_data);
    amxc_var_copy(&copy_data, data);
    notification = extract_notification(sub_ref, &copy_data, 1);

    assert_non_null(notification);
    assert_true(test_verify_data(notification, "value_change.param_path", "Phonebook.Contact.1.FirstName"));
    assert_true(test_verify_data(notification, "value_change.param_value", "Vesper"));
    assert_true(test_verify_data(notification, "subscription_id", "vc-param"));
    amxc_var_delete(&notification);

    notification = extract_notification(sub_ref, &copy_data, -1);
    assert_null(notification);

    amxc_var_clean(&copy_data);
}

static void build_obj_event(UNUSED const char* const sig_name,
                            const amxc_var_t* const data,
                            void* const priv) {
    uspi_sub_ref_t* sub_ref = (uspi_sub_ref_t*) priv;
    amxc_var_t* notification = NULL;
    amxc_var_t copy_data;

    amxc_var_init(&copy_data);
    amxc_var_copy(&copy_data, data);
    notification = extract_notification(sub_ref, &copy_data, 0);

    assert_true(test_verify_data(notification, "event.event_name", "IncomingCall!"));
    assert_true(test_verify_data(notification, "event.path", "Phonebook.Contact.1."));
    assert_true(test_verify_data(notification, "subscription_id", "event"));

    amxc_var_clean(&copy_data);
    amxc_var_delete(&notification);
}

static void build_obj_deletion(UNUSED const char* const sig_name,
                               const amxc_var_t* const data,
                               void* const priv) {
    uspi_sub_ref_t* sub_ref = (uspi_sub_ref_t*) priv;
    amxc_var_t* notification = NULL;
    amxc_var_t copy_data;

    amxc_var_init(&copy_data);
    amxc_var_copy(&copy_data, data);
    notification = extract_notification(sub_ref, &copy_data, 0);

    assert_true(test_verify_data(notification, "obj_deletion.path", "Phonebook.Contact.1."));
    assert_true(test_verify_data(notification, "subscription_id", "obj-deletion"));

    amxc_var_clean(&copy_data);
    amxc_var_delete(&notification);
}

static void retry_cb(UNUSED amxp_timer_t* timer, UNUSED void* priv) {
    uspi_sub_retry_t* sub_rety = (uspi_sub_retry_t*) priv;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t notification;

    printf("Retry notification\n");
    fflush(stdout);

    amxc_var_init(&notification);

    usp_rx = uspl_msghandler_unpack_protobuf(sub_rety->usp_tx->pbuf, sub_rety->usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    assert_int_equal(uspl_notify_extract(usp_rx, &notification), 0);
    amxc_var_dump(&notification, STDOUT_FILENO);

    assert_true(test_verify_data(&notification, "value_change.param_path", "Phonebook.Contact.1.FirstName"));
    assert_true(test_verify_data(&notification, "value_change.param_value", "Ernst"));
    assert_true(test_verify_data(&notification, "subscription_id", "sub-retry"));

    amxc_var_clean(&notification);
}

static void build_vc_retry(UNUSED const char* const sig_name,
                           const amxc_var_t* const data,
                           void* const priv) {
    uspi_sub_ref_t* sub_ref = (uspi_sub_ref_t*) priv;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t notification;
    amxc_var_t copy_data;

    amxc_var_dump(data, STDOUT_FILENO);
    amxc_var_init(&notification);
    amxc_var_init(&copy_data);
    amxc_var_copy(&copy_data, data);

    uspl_tx_new(&usp_tx, from_id, to_id);
    uspi_subscription_build_notify(&copy_data, sub_ref, usp_tx, false);

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    assert_int_equal(uspl_notify_extract(usp_rx, &notification), 0);
    amxc_var_dump(&notification, STDOUT_FILENO);

    assert_true(test_verify_data(&notification, "value_change.param_path", "Phonebook.Contact.1.FirstName"));
    assert_true(test_verify_data(&notification, "value_change.param_value", "Ernst"));
    assert_true(test_verify_data(&notification, "subscription_id", "sub-retry"));

    uspi_sub_retry_new(sub_ref->sub, usp_tx, retry_cb, bus_ctx);

    amxc_var_clean(&copy_data);
    amxc_var_clean(&notification);
    uspl_rx_delete(&usp_rx);
}

static void build_amx_notif(UNUSED const char* const sig_name,
                            const amxc_var_t* const data,
                            void* const priv) {
    uspi_sub_ref_t* sub_ref = (uspi_sub_ref_t*) priv;
    int result = 0;
    amxc_var_t* notification = NULL;
    amxc_var_t* amx_notification = NULL;
    amxc_var_t copy_data;

    amxc_var_init(&copy_data);
    amxc_var_copy(&copy_data, data);
    notification = extract_notification(sub_ref, &copy_data, 0);
    amx_notification = GET_ARG(notification, "amx_notification");

    assert_int_equal(amxc_var_compare(data, amx_notification, &result), 0);
    assert_int_equal(result, 0);

    amxc_var_clean(&copy_data);
    amxc_var_delete(&notification);
}

int test_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);
    assert_int_equal(test_register_dummy_be(), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    amxo_connection_add(&parser, 101, connection_read, "dummy:/tmp/dummy.sock", AMXO_BUS, bus_ctx);

    amxb_register(bus_ctx, &dm);
    capture_sigalrm();

    assert_int_equal(amxo_parser_parse_file(&parser, phonebook_definition, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, localagent_definition, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, localagent_defaults, root_obj), 0);

    return 0;
}

int test_teardown(UNUSED void** state) {
    amxb_free(&bus_ctx);

    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    test_unregister_dummy_be();
    return 0;
}

void test_uspi_subs_obj_creation(UNUSED void** state) {
    uspi_sub_t* sub = NULL;
    uspi_sub_ref_t* sub_ref = NULL;
    const char* object = "Phonebook.Contact.";
    char* expr = NULL;
    amxd_path_t path;

    amxd_path_init(&path, object);

    uspi_sub_new(&sub, subs_inst, contr_inst, "obj-creation", "ObjectCreation");
    sub_ref = uspi_sub_ref_new(sub, object);
    expr = uspi_subscription_build_expression(&path, "ObjectCreation");
    printf("Expression = %s\n", expr);
    fflush(stdout);
    assert_string_equal(expr, "(notification == 'dm:instance-added')");

    amxb_subscribe(bus_ctx, object, expr, build_obj_creation, sub_ref);

    assert_int_equal(amxb_add(bus_ctx, "Phonebook.Contact.", 0, "contact-1", NULL, NULL, 5), 0);
    handle_events();

    assert_int_equal(amxb_unsubscribe(bus_ctx, object, build_obj_creation, sub_ref), 0);

    free(expr);
    amxd_path_clean(&path);
    uspi_sub_delete(&sub);
}

void test_uspi_subs_vc_object(UNUSED void** state) {
    uspi_sub_t* sub = NULL;
    uspi_sub_ref_t* sub_ref = NULL;
    const char* object = "Phonebook.Contact.1.";
    char* expr = NULL;
    amxc_var_t args;
    amxd_path_t path;

    amxc_var_init(&args);
    amxd_path_init(&path, object);

    uspi_sub_new(&sub, subs_inst, contr_inst, "vc-object", "ValueChange");
    sub_ref = uspi_sub_ref_new(sub, object);
    expr = uspi_subscription_build_expression(&path, "ValueChange");
    printf("Expression = %s\n", expr);
    fflush(stdout);
    assert_string_equal(expr, "(notification == 'dm:object-changed')");

    amxb_subscribe(bus_ctx, object, expr, build_vc_object, sub_ref);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "FirstName", "James");
    amxc_var_add_key(cstring_t, &args, "LastName", "Bond");
    assert_int_equal(amxb_set(bus_ctx, object, &args, NULL, 5), 0);
    handle_events();

    assert_int_equal(amxb_unsubscribe(bus_ctx, object, build_vc_object, sub_ref), 0);

    free(expr);
    amxd_path_clean(&path);
    amxc_var_clean(&args);
    uspi_sub_delete(&sub);
}

void test_uspi_subs_vc_single_param(UNUSED void** state) {
    uspi_sub_t* sub = NULL;
    uspi_sub_ref_t* sub_ref = NULL;
    const char* object = "Phonebook.Contact.1.";
    const char* param_path = "Phonebook.Contact.1.FirstName";
    char* expr = NULL;
    amxc_var_t args;
    amxd_path_t path;

    amxc_var_init(&args);
    amxd_path_init(&path, param_path);

    uspi_sub_new(&sub, subs_inst, contr_inst, "vc-param", "ValueChange");
    sub_ref = uspi_sub_ref_new(sub, param_path);
    expr = uspi_subscription_build_expression(&path, "ValueChange");
    printf("Expression = %s\n", expr);
    fflush(stdout);
    assert_string_equal(expr, "(notification == 'dm:object-changed') && contains('parameters.FirstName')");

    amxb_subscribe(bus_ctx, object, expr, build_vc_single_param, sub_ref);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "FirstName", "Vesper");
    amxc_var_add_key(cstring_t, &args, "LastName", "Lynd");
    assert_int_equal(amxb_set(bus_ctx, object, &args, NULL, 5), 0);
    handle_events();

    assert_int_equal(amxb_unsubscribe(bus_ctx, object, build_vc_single_param, sub_ref), 0);

    free(expr);
    amxd_path_clean(&path);
    amxc_var_clean(&args);
    uspi_sub_delete(&sub);
}

void test_uspi_subs_event(UNUSED void** state) {
    uspi_sub_t* sub = NULL;
    uspi_sub_ref_t* sub_ref = NULL;
    const char* object = "Phonebook.Contact.1.";
    const char* event_path = "Phonebook.Contact.1.IncomingCall!";
    char* expr = NULL;
    amxd_object_t* obj = amxd_dm_findf(&dm, "%s", object);
    amxd_path_t path;
    amxc_var_t data;

    amxd_path_init(&path, event_path);
    amxc_var_init(&data);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);

    uspi_sub_new(&sub, subs_inst, contr_inst, "event", "Event");
    sub_ref = uspi_sub_ref_new(sub, event_path);
    expr = uspi_subscription_build_expression(&path, "Event");
    printf("Expression = %s\n", expr);
    fflush(stdout);
    assert_string_equal(expr, "(notification == 'IncomingCall!')");

    amxb_subscribe(bus_ctx, object, expr, build_obj_event, sub_ref);

    amxc_var_add_key(uint32_t, &data, "CallerID", 1234);
    amxd_object_send_signal(obj, "IncomingCall!", &data, false);
    handle_events();

    assert_int_equal(amxb_unsubscribe(bus_ctx, object, build_obj_event, sub_ref), 0);

    free(expr);
    amxc_var_clean(&data);
    amxd_path_clean(&path);
    uspi_sub_delete(&sub);
}

void test_uspi_notification_retry(UNUSED void** state) {
    uspi_sub_t* sub = NULL;
    uspi_sub_ref_t* sub_ref = NULL;
    const char* object = "Phonebook.Contact.1.";
    const char* param_path = "Phonebook.Contact.1.FirstName";
    char* expr = NULL;
    amxc_var_t args;
    amxd_path_t path;

    amxc_var_init(&args);
    amxd_path_init(&path, param_path);

    uspi_sub_new(&sub, subs_inst, contr_inst, "sub-retry", "ValueChange");
    sub_ref = uspi_sub_ref_new(sub, param_path);
    expr = uspi_subscription_build_expression(&path, "ValueChange");
    printf("Expression = %s\n", expr);
    fflush(stdout);
    assert_string_equal(expr, "(notification == 'dm:object-changed') && contains('parameters.FirstName')");

    amxb_subscribe(bus_ctx, object, expr, build_vc_retry, sub_ref);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "FirstName", "Ernst");
    amxc_var_add_key(cstring_t, &args, "LastName", "Blofeld");
    assert_int_equal(amxb_set(bus_ctx, object, &args, NULL, 5), 0);
    handle_events();

    read_sigalrm();

    assert_int_equal(amxb_unsubscribe(bus_ctx, object, build_vc_retry, sub_ref), 0);

    free(expr);
    amxd_path_clean(&path);
    amxc_var_clean(&args);
    uspi_sub_delete(&sub);
}

void test_uspi_subs_obj_deletion(UNUSED void** state) {
    uspi_sub_t* sub = NULL;
    uspi_sub_ref_t* sub_ref = NULL;
    const char* object = "Phonebook.Contact.";
    char* expr = NULL;
    amxd_path_t path;

    amxd_path_init(&path, object);

    uspi_sub_new(&sub, subs_inst, contr_inst, "obj-deletion", "ObjectDeletion");
    sub_ref = uspi_sub_ref_new(sub, object);
    expr = uspi_subscription_build_expression(&path, "ObjectDeletion");
    printf("Expression = %s\n", expr);
    fflush(stdout);
    assert_string_equal(expr, "(notification == 'dm:instance-removed')");

    amxb_subscribe(bus_ctx, object, expr, build_obj_deletion, sub_ref);

    assert_int_equal(amxb_del(bus_ctx, object, 0, "contact-1", NULL, 5), 0);
    handle_events();

    assert_int_equal(amxb_unsubscribe(bus_ctx, object, build_obj_deletion, sub_ref), 0);

    free(expr);
    amxd_path_clean(&path);
    uspi_sub_delete(&sub);
}

void test_uspi_subs_amx_obj_creation(UNUSED void** state) {
    uspi_sub_t* sub = NULL;
    uspi_sub_ref_t* sub_ref = NULL;
    const char* object = "Phonebook.Contact.*.PhoneNumber.";
    char* expr = NULL;
    amxd_path_t path;
    amxc_var_t values;

    amxc_var_init(&values);
    amxd_path_init(&path, object);

    uspi_sub_new(&sub, subs_inst, contr_inst, "any-obj-creation", "AmxNotification");
    sub_ref = uspi_sub_ref_new(sub, object);
    expr = uspi_subscription_build_expression(&path, NULL);
    printf("Expression = %s\n", expr);
    fflush(stdout);
    assert_null(expr);

    // Make sure an instance exists
    assert_int_equal(amxb_add(bus_ctx, "Phonebook.Contact.", 1, "contact-1", NULL, NULL, 5), 0);

    assert_int_equal(amxb_subscribe(bus_ctx, object, expr, build_amx_notif, sub_ref), 0);

    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &values, "Phone", "123");
    assert_int_equal(amxb_add(bus_ctx, "Phonebook.Contact.1.PhoneNumber.", 1, "phone-1", &values, NULL, 5), 0);
    handle_events();

    assert_int_equal(amxb_unsubscribe(bus_ctx, object, build_amx_notif, sub_ref), 0);

    amxc_var_clean(&values);
    amxd_path_clean(&path);
    uspi_sub_delete(&sub);
}

void test_uspi_subs_amx_vc(UNUSED void** state) {
    uspi_sub_t* sub = NULL;
    uspi_sub_ref_t* sub_ref = NULL;
    const char* object = "Phonebook.Contact.*.PhoneNumber.*.";
    char* expr = NULL;
    amxd_path_t path;
    amxc_var_t args;

    amxc_var_init(&args);
    amxd_path_init(&path, object);

    uspi_sub_new(&sub, subs_inst, contr_inst, "any-vc", "AmxNotification");
    sub_ref = uspi_sub_ref_new(sub, object);
    expr = uspi_subscription_build_expression(&path, NULL);
    printf("Expression = %s\n", expr);
    fflush(stdout);
    assert_null(expr);

    assert_int_equal(amxb_subscribe(bus_ctx, object, expr, build_amx_notif, sub_ref), 0);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "Phone", "456");
    amxc_var_add_key(uint32_t, &args, "Priority", 1);
    assert_int_equal(amxb_set(bus_ctx, object, &args, NULL, 5), 0);
    handle_events();

    assert_int_equal(amxb_unsubscribe(bus_ctx, object, build_amx_notif, sub_ref), 0);

    amxc_var_clean(&args);
    amxd_path_clean(&path);
    uspi_sub_delete(&sub);
}

void test_uspi_subs_amx_event(UNUSED void** state) {
    uspi_sub_t* sub = NULL;
    uspi_sub_ref_t* sub_ref = NULL;
    const char* object = "Phonebook.Contact.1.";
    const char* event_path = "Phonebook.Contact.1.IncomingCall!";
    char* expr = NULL;
    amxd_object_t* obj = amxd_dm_findf(&dm, "%s", object);
    amxd_path_t path;
    amxc_var_t data;

    amxd_path_init(&path, event_path);
    amxc_var_init(&data);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);

    uspi_sub_new(&sub, subs_inst, contr_inst, "any-event", "AmxNotification");
    sub_ref = uspi_sub_ref_new(sub, event_path);
    expr = uspi_subscription_build_expression(&path, NULL);
    printf("Expression = %s\n", expr);
    fflush(stdout);
    assert_null(expr);

    assert_int_equal(amxb_subscribe(bus_ctx, object, expr, build_amx_notif, sub_ref), 0);

    amxc_var_add_key(uint32_t, &data, "CallerID", 1234);
    amxd_object_send_signal(obj, "IncomingCall!", &data, false);
    handle_events();

    assert_int_equal(amxb_unsubscribe(bus_ctx, object, build_amx_notif, sub_ref), 0);

    amxc_var_clean(&data);
    amxd_path_clean(&path);
    uspi_sub_delete(&sub);
}

void test_uspi_subs_amx_obj_deletion(UNUSED void** state) {
    uspi_sub_t* sub = NULL;
    uspi_sub_ref_t* sub_ref = NULL;
    const char* object = "Phonebook.Contact.*.PhoneNumber.";
    char* expr = NULL;
    amxd_path_t path;
    amxc_var_t values;

    amxc_var_init(&values);
    amxd_path_init(&path, object);

    uspi_sub_new(&sub, subs_inst, contr_inst, "any-obj-deletion", "AmxNotification");
    sub_ref = uspi_sub_ref_new(sub, object);
    expr = uspi_subscription_build_expression(&path, NULL);
    printf("Expression = %s\n", expr);
    fflush(stdout);
    assert_null(expr);

    assert_int_equal(amxb_subscribe(bus_ctx, object, expr, build_amx_notif, sub_ref), 0);

    assert_int_equal(amxb_del(bus_ctx, "Phonebook.Contact.1.PhoneNumber.", 1, "phone-1", NULL, 5), 0);
    handle_events();

    assert_int_equal(amxb_unsubscribe(bus_ctx, object, build_amx_notif, sub_ref), 0);
    handle_events();

    amxc_var_clean(&values);
    amxd_path_clean(&path);
    uspi_sub_delete(&sub);
}

static void test_convert_value_change_event(amxc_var_t* dst, const amxc_var_t* const src) {
    amxc_var_t* parameter = GETP_ARG(src, "parameters.0");

    amxc_var_set_type(dst, AMXC_VAR_ID_HTABLE);
    amxc_var_t* f = amxc_var_add_new_key(dst, "from");
    amxc_var_t* t = amxc_var_add_new_key(dst, "to");
    amxc_var_add_key(cstring_t, dst, "path", GET_CHAR(src, "path"));
    amxc_var_add_key(cstring_t, dst, "param_name", amxc_var_key(parameter));
    amxc_var_copy(f, GET_ARG(parameter, "from"));
    amxc_var_copy(t, GET_ARG(parameter, "to"));
    amxc_var_dump(dst, STDOUT_FILENO);
}

static void handle_event_v2(UNUSED const char* const sig_name,
                            const amxc_var_t* const data,
                            void* const priv) {
    uspl_tx_t* usp_tx = (uspl_tx_t*) priv;
    const char* notification = GET_CHAR(data, "notification");
    if(strcmp(notification, "dm:object-changed") == 0) {
        amxc_var_t converted;
        amxc_var_init(&converted);
        test_convert_value_change_event(&converted, data);
        uspi_subscription_build_notify_v2(&converted, notify_value_changed, "123", usp_tx, false);
        amxc_var_clean(&converted);
    } else if(strcmp(notification, "dm:instance-added") == 0) {
        uspi_subscription_build_notify_v2(data, notify_object_creation, "123", usp_tx, false);
    } else if(strcmp(notification, "dm:instance-removed") == 0) {
        uspi_subscription_build_notify_v2(data, notify_object_deletion, "123", usp_tx, false);
    }
}

void test_uspi_subs_value_change_v2(UNUSED void** state) {
    const char* object = "Phonebook.Contact.1.";
    const char* filename = "./results/value_change_v2.json";
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t args;
    amxc_var_t result;

    amxc_var_init(&args);
    amxc_var_init(&result);
    uspl_tx_new(&usp_tx, "proto::agent", "proto::controller");

    assert_int_equal(amxb_subscribe(bus_ctx, object, NULL, handle_event_v2, usp_tx), 0);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "FirstName", "Q");
    assert_int_equal(amxb_set(bus_ctx, object, &args, NULL, 5), 0);
    handle_events();

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    assert_int_equal(uspl_notify_extract(usp_rx, &result), 0);
    amxc_var_dump(&result, STDOUT_FILENO);
    //amxut_util_write_to_json_file(&result, filename);
    amxut_verify_variant_from_json_file(&result, filename);

    assert_int_equal(amxb_unsubscribe(bus_ctx, object, handle_event_v2, usp_tx), 0);

    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
    amxc_var_clean(&result);
    amxc_var_clean(&args);
}

void test_uspi_subs_obj_creation_v2(UNUSED void** state) {
    const char* object = "Phonebook.Contact.";
    const char* filename = "./results/obj_creation_v2.json";
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t result;

    amxc_var_init(&result);
    uspl_tx_new(&usp_tx, "proto::agent", "proto::controller");

    assert_int_equal(amxb_subscribe(bus_ctx, object, NULL, handle_event_v2, usp_tx), 0);

    assert_int_equal(amxb_add(bus_ctx, "Phonebook.Contact.", 0, "contact-2", NULL, NULL, 5), 0);
    handle_events();

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    assert_int_equal(uspl_notify_extract(usp_rx, &result), 0);
    amxc_var_dump(&result, STDOUT_FILENO);
    //amxut_util_write_to_json_file(&result, filename);
    amxut_verify_variant_from_json_file(&result, filename);

    assert_int_equal(amxb_unsubscribe(bus_ctx, object, handle_event_v2, usp_tx), 0);

    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
    amxc_var_clean(&result);
}

void test_uspi_subs_obj_deletion_v2(UNUSED void** state) {
    const char* object = "Phonebook.Contact.";
    const char* filename = "./results/obj_deletion_v2.json";
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t result;

    amxc_var_init(&result);
    uspl_tx_new(&usp_tx, "proto::agent", "proto::controller");

    assert_int_equal(amxb_subscribe(bus_ctx, object, NULL, handle_event_v2, usp_tx), 0);

    assert_int_equal(amxb_del(bus_ctx, object, 0, "contact-2", NULL, 5), 0);
    handle_events();

    usp_rx = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_rx);

    assert_int_equal(uspl_notify_extract(usp_rx, &result), 0);
    amxc_var_dump(&result, STDOUT_FILENO);
    //amxut_util_write_to_json_file(&result, filename);
    amxut_verify_variant_from_json_file(&result, filename);

    assert_int_equal(amxb_unsubscribe(bus_ctx, object, handle_event_v2, usp_tx), 0);

    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
    amxc_var_clean(&result);
}
