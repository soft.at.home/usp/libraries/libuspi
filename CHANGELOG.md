# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v1.1.1 - 2024-09-30(05:52:45 +0000)

### Other

- CI: Disable squashing of open source commits
- [AMX] Disable commit squashing

## Release v1.1.0 - 2024-07-12(08:55:32 +0000)

### New

- [TR181-Device]Bidirectional communication support between UBUS and IMTP

## Release v1.0.4 - 2024-04-30(13:47:25 +0000)

### Other

- Move component to prpl-foundation gitlab

## Release v1.0.3 - 2024-01-09(10:06:31 +0000)

### Other

- Remove circular dependency

## Release v1.0.2 - 2023-12-18(13:46:39 +0000)

### Other

- [USP] Enable documentation generation for libuspi

## Release v1.0.1 - 2023-08-17(06:58:53 +0000)

### Fixes

- [USP] messages are not published sometimes

## Release v1.0.0 - 2023-07-10(13:12:35 +0000)

### Breaking

- [IMTP] Implement IMTP communication as specified in TR-369

## Release v0.5.0 - 2023-04-26(09:05:43 +0000)

### New

- [USP] Add NotifType AmxNotification for ambiorix events

## Release v0.4.0 - 2023-04-13(11:20:17 +0000)

### Changes

- [USP] Port subscription changes to libuspi

## Release v0.3.0 - 2023-03-22(08:26:35 +0000)

### New

- [USP] Add support for subscriptions to usp-endpoint

## Release v0.2.1 - 2022-09-06(07:51:42 +0000)

### Other

- [USP] libuspi needs to be open sourced

## Release v0.2.0 - 2022-09-05(09:34:44 +0000)

### New

- A signal must be emitted after adding an EndpointID

## Release v0.1.1 - 2022-08-29(09:15:02 +0000)

### Fixes

- Remove pedantic flag

## Release v0.1.0 - 2022-08-29(08:06:09 +0000)

### New

- Implement common API for connecting over the IMTP, handling messages and registering data models

