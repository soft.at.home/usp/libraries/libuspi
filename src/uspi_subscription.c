/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <yajl/yajl_gen.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxj/amxj_variant.h>
#include <amxp/amxp.h>
#include <amxd/amxd_object.h>
#include <amxb/amxb.h>

#include "uspi.h"
#include "uspi/uspi_utils.h"
#include "uspi/uspi_subscription.h"
#include "uspi/uspi_prefix.h"

typedef int (* uspi_request_fn_t) (amxc_var_t*, uspl_tx_t*, uspi_sub_ref_t*, bool);

typedef struct _uspi_request_builder {
    const char* notification_type;
    uspi_request_fn_t fn;
} uspi_request_builder_t;

static void uspi_sub_retry_list_it_delete(amxc_llist_it_t* it) {
    uspi_sub_retry_t* sub_retry = amxc_container_of(it, uspi_sub_retry_t, lit);
    uspi_sub_retry_delete(&sub_retry);
}

static uint32_t uspi_calc_retry_timeout(uspi_sub_t* sub, amxb_bus_ctx_t* bus_ctx) {
    int retval = -1;
    uint32_t interval_mult = 0;
    uint32_t interval_min = 0;
    uint32_t interval_max = 0;
    uint32_t result = 0;
    amxc_var_t ret;

    amxc_var_init(&ret);

    retval = amxb_get(bus_ctx, sub->contr_path, 0, &ret, 5);
    when_failed(retval, exit);

    interval_mult = GETP_UINT32(&ret, "0.0.USPNotifRetryIntervalMultiplier");
    interval_min = GETP_UINT32(&ret, "0.0.USPNotifRetryMinimumWaitInterval");

    interval_max = interval_min * interval_mult / 1000;
    srand(time(0));
    result = (rand() % (interval_max - interval_min)) + interval_min;

exit:
    amxc_var_clean(&ret);

    return result;
}

static void uspi_subscription_expr_event(amxd_path_t* path, amxc_string_t* expr) {
    const char* event = amxd_path_get_param(path);

    if((event != NULL) && (*event != 0)) {
        amxc_string_setf(expr, "(notification == '%s')", event);
    } else {
        amxc_string_setf(expr, "notification matches '.*!$'");
    }
}

static amxc_var_t* uspi_subs_vc_get_param(amxc_var_t* sig_params, const char* ref_path) {
    amxd_path_t path;
    const char* param = NULL;
    amxc_var_t* result = NULL;

    amxd_path_init(&path, ref_path);
    param = amxd_path_get_param(&path);
    if(param != NULL) {
        result = GET_ARG(sig_params, param);
    } else {
        result = GETI_ARG(sig_params, 0);
    }

    amxd_path_clean(&path);
    return result;
}

static void notif_expire(UNUSED amxp_timer_t* timer, void* priv) {
    SAH_TRACEZ_INFO(ME, "Notification expired");
    uspi_sub_retry_t* sub_retry = (uspi_sub_retry_t*) priv;
    uspi_sub_retry_delete(&sub_retry);
}

static void uspi_sub_ref_list_it_delete(amxc_llist_it_t* it) {
    uspi_sub_ref_t* sub_ref = amxc_container_of(it, uspi_sub_ref_t, lit);
    free(sub_ref->ref_path);
    free(sub_ref);
}

void uspi_sub_retry_new(uspi_sub_t* sub,
                        uspl_tx_t* usp_tx,
                        amxp_timer_cb_t timer_cb,
                        amxb_bus_ctx_t* bus_ctx) {
    uspi_sub_retry_t* sub_retry = NULL;
    amxc_var_t* notif_expiration_var = NULL;
    uint32_t notif_expiration = 0;

    sub_retry = calloc(1, sizeof(uspi_sub_retry_t));
    when_null(sub_retry, exit);

    amxc_llist_append(&sub->retry_list, &sub_retry->lit);

    sub_retry->usp_tx = usp_tx;
    notif_expiration_var = uspi_utils_get_param_value(sub->instance_path, "NotifExpiration");
    if(notif_expiration_var != NULL) {
        notif_expiration = amxc_var_dyncast(uint32_t, notif_expiration_var);
    }

    amxp_timer_new(&sub_retry->retry_timer, timer_cb, sub_retry);
    sub_retry->retry_time = uspi_calc_retry_timeout(sub, bus_ctx);
    SAH_TRACEZ_NOTICE(ME, "Next USP notification retry with id %s in %d seconds",
                      sub->id, sub_retry->retry_time);
    amxp_timer_start(sub_retry->retry_timer, 1000 * sub_retry->retry_time);
    if(notif_expiration > 0) {
        amxp_timer_new(&sub_retry->expiration_timer, notif_expire, sub_retry);
        amxp_timer_start(sub_retry->expiration_timer, 1000 * notif_expiration);
    }

exit:
    amxc_var_delete(&notif_expiration_var);
    return;
}

void uspi_sub_retry_delete(uspi_sub_retry_t** sub_retry) {
    when_null(sub_retry, exit);
    when_null(*sub_retry, exit);

    amxc_llist_it_take(&(*sub_retry)->lit);
    amxp_timer_delete(&(*sub_retry)->retry_timer);
    amxp_timer_delete(&(*sub_retry)->expiration_timer);
    uspl_tx_delete(&(*sub_retry)->usp_tx);
    free(*sub_retry);

exit:
    return;
}

int uspi_sub_new(uspi_sub_t** sub,
                 const char* inst_path,
                 const char* contr_path,
                 const char* id,
                 const char* notif_type) {
    int retval = -1;
    amxc_string_t path;

    amxc_string_init(&path, 0);

    when_null(sub, exit);
    when_str_empty(inst_path, exit);
    when_str_empty(contr_path, exit);
    when_str_empty(id, exit);
    when_str_empty(notif_type, exit);

    *sub = calloc(1, sizeof(uspi_sub_t));
    when_null((*sub), exit);

    (*sub)->instance_path = strdup(inst_path);
    if((*sub)->instance_path == NULL) {
        free(*sub);
        goto exit;
    }

    amxc_string_setf(&path, "%s", contr_path);
    uspi_prefix_strip_device(&path);
    (*sub)->contr_path = amxc_string_take_buffer(&path);

    (*sub)->id = strdup(id);
    if((*sub)->id == NULL) {
        free((*sub)->contr_path);
        free((*sub)->instance_path);
        free(*sub);
        goto exit;
    }

    (*sub)->notif_type = strdup(notif_type);
    if((*sub)->notif_type == NULL) {
        free((*sub)->id);
        free((*sub)->contr_path);
        free((*sub)->instance_path);
        free(*sub);
        goto exit;
    }

    amxc_llist_init(&(*sub)->sub_ref_list);
    amxc_llist_init(&(*sub)->retry_list);

    retval = 0;
exit:
    amxc_string_clean(&path);
    return retval;
}

void uspi_sub_delete(uspi_sub_t** sub) {
    when_null(sub, exit);
    when_null(*sub, exit);

    amxp_timer_delete(&(*sub)->periodic_timer);
    amxc_llist_clean(&(*sub)->retry_list, uspi_sub_retry_list_it_delete);
    amxc_llist_clean(&(*sub)->sub_ref_list, uspi_sub_ref_list_it_delete);
    free((*sub)->instance_path);
    free((*sub)->contr_path);
    free((*sub)->id);
    free((*sub)->notif_type);
    free((*sub));
    *sub = NULL;

exit:
    return;
}

uspi_sub_ref_t* uspi_sub_ref_new(uspi_sub_t* sub, const char* ref_path) {
    uspi_sub_ref_t* sub_ref = (uspi_sub_ref_t*) calloc(1, sizeof(uspi_sub_ref_t));
    when_null(sub_ref, exit);

    sub_ref->ref_path = strdup(ref_path);
    if(sub_ref->ref_path == NULL) {
        free(sub_ref);
        sub_ref = NULL;
        goto exit;
    }
    sub_ref->sub = sub;
    amxc_llist_append(&sub->sub_ref_list, &sub_ref->lit);

exit:
    return sub_ref;
}

uspi_sub_ref_t* uspi_sub_ref_find(uspi_sub_t* sub, const char* ref_path) {
    uspi_sub_ref_t* result = NULL;

    when_null(sub, exit);
    when_str_empty(ref_path, exit);

    amxc_llist_for_each(lit, &sub->sub_ref_list) {
        uspi_sub_ref_t* sub_ref = amxc_llist_it_get_data(lit, uspi_sub_ref_t, lit);
        if(strcmp(sub_ref->ref_path, ref_path) == 0) {
            result = sub_ref;
            break;
        }
    }

exit:
    return result;
}

char* uspi_subscription_build_expression(amxd_path_t* path,
                                         const char* notif_type) {
    char* expr = NULL;
    amxc_string_t expr_str;
    amxc_string_init(&expr_str, 0);
    const char* param = amxd_path_get_param(path);

    when_null(path, exit);
    when_str_empty(notif_type, exit);

    if(strcmp("ValueChange", notif_type) == 0) {
        amxc_string_setf(&expr_str, "(notification == 'dm:object-changed')");
    } else if(strcmp("ObjectCreation", notif_type) == 0) {
        amxc_string_setf(&expr_str, "(notification == 'dm:instance-added')");
    } else if(strcmp("ObjectDeletion", notif_type) == 0) {
        amxc_string_setf(&expr_str, "(notification == 'dm:instance-removed')");
    } else if(strcmp("Event", notif_type) == 0) {
        uspi_subscription_expr_event(path, &expr_str);
        expr = amxc_string_take_buffer(&expr_str);
        goto exit;
    } else {
        goto exit;
    }

    if(param != NULL) {
        amxc_string_appendf(&expr_str, " && contains('parameters.%s')", param);
    }

    expr = amxc_string_take_buffer(&expr_str);

exit:
    amxc_string_clean(&expr_str);
    return expr;
}

int uspi_subscription_build_value_change(amxc_var_t* data,
                                         uspl_tx_t* usp_tx,
                                         uspi_sub_ref_t* sub_ref,
                                         bool send_resp) {
    int retval = 1;
    amxc_var_t request;
    amxc_string_t param_path;
    char* param_value = NULL;
    const char* param_name = NULL;
    const char* path = GET_CHAR(data, "path");
    uspi_sub_t* sub = sub_ref->sub;
    amxc_var_t* sig_params = GET_ARG(data, "parameters");
    amxc_var_t* value_change = NULL;
    amxc_var_t* param = NULL;
    amxc_var_t* param_path_var = NULL;

    amxc_var_init(&request);
    amxc_string_init(&param_path, 0);

    amxc_var_set_type(&request, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, &request, "notification_case", USP__NOTIFY__NOTIFICATION_VALUE_CHANGE);
    amxc_var_add_key(cstring_t, &request, "subscription_id", sub->id);
    amxc_var_add_key(bool, &request, "send_resp", send_resp);
    value_change = amxc_var_add_key(amxc_htable_t, &request, "value_change", NULL);

    param = uspi_subs_vc_get_param(sig_params, sub_ref->ref_path);
    when_null_status(param, exit, retval = -1);

    param_name = amxc_var_key(param);
    param_value = amxc_var_dyncast(cstring_t, GET_ARG(param, "to"));

    amxc_string_setf(&param_path, "%s%s", path, param_name);
    param_path_var = amxc_var_add_new_key(value_change, "param_path");
    amxc_var_push(cstring_t, param_path_var, amxc_string_take_buffer(&param_path));
    amxc_var_add_key(cstring_t, value_change, "param_value", param_value);

    SAH_TRACEZ_INFO(ME, "Build notification of type ValueChange for %s = %s",
                    amxc_var_constcast(cstring_t, param_path_var), param_value);
    uspl_notify_new(usp_tx, &request);

exit:
    amxc_var_delete(&param);
    if(amxc_htable_is_empty(amxc_var_constcast(amxc_htable_t, sig_params))) {
        retval = 0;
    }
    free(param_value);
    amxc_var_clean(&request);
    amxc_string_clean(&param_path);
    return retval;
}

static int uspi_subscription_build_value_change_v2(const amxc_var_t* const data,
                                                   uspl_tx_t* usp_tx,
                                                   const char* id,
                                                   bool send_resp) {
    int retval = -1;
    amxc_var_t request;
    amxc_string_t param_path;
    const char* path = GET_CHAR(data, "path");
    const char* param_name = GET_CHAR(data, "param_name");
    char* param_value = amxc_var_dyncast(cstring_t, GET_ARG(data, "to"));
    amxc_var_t* value_change = NULL;
    amxc_var_t* param_path_var = NULL;
    amxc_var_t* param_value_var = NULL;

    amxc_var_init(&request);
    amxc_string_init(&param_path, 0);

    amxc_var_set_type(&request, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, &request, "notification_case", USP__NOTIFY__NOTIFICATION_VALUE_CHANGE);
    amxc_var_add_key(cstring_t, &request, "subscription_id", id);
    amxc_var_add_key(bool, &request, "send_resp", send_resp);
    value_change = amxc_var_add_key(amxc_htable_t, &request, "value_change", NULL);

    amxc_string_setf(&param_path, "%s%s", path, param_name);
    param_path_var = amxc_var_add_new_key(value_change, "param_path");
    amxc_var_push(cstring_t, param_path_var, amxc_string_take_buffer(&param_path));
    param_value_var = amxc_var_add_new_key(value_change, "param_value");
    amxc_var_push(cstring_t, param_value_var, param_value);

    SAH_TRACEZ_INFO(ME, "Build notification of type ValueChange for %s = %s",
                    amxc_var_constcast(cstring_t, param_path_var), param_value);
    retval = uspl_notify_new(usp_tx, &request);

    amxc_var_clean(&request);
    amxc_string_clean(&param_path);
    return retval;
}

static int uspi_subscription_build_object_creation_common(const amxc_var_t* const data,
                                                          uspl_tx_t* usp_tx,
                                                          const char* id,
                                                          bool send_resp) {
    int retval = -1;
    amxc_var_t request;
    amxc_var_t* keys = GET_ARG(data, "keys");
    const char* path = GET_CHAR(data, "path");
    amxc_var_t* obj_creation = NULL;
    amxc_var_t* path_var = NULL;
    amxc_string_t instance_path;

    amxc_string_init(&instance_path, 0);
    amxc_string_setf(&instance_path, "%s%d.", path, GET_UINT32(data, "index"));
    amxc_var_init(&request);
    amxc_var_set_type(&request, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, &request, "notification_case", USP__NOTIFY__NOTIFICATION_OBJ_CREATION);
    amxc_var_add_key(cstring_t, &request, "subscription_id", id);
    amxc_var_add_key(bool, &request, "send_resp", send_resp);
    obj_creation = amxc_var_add_key(amxc_htable_t, &request, "obj_creation", NULL);

    amxc_var_add_key(amxc_htable_t, obj_creation, "keys", amxc_var_constcast(amxc_htable_t, keys));
    path_var = amxc_var_add_new_key(obj_creation, "path");
    amxc_var_push(cstring_t, path_var, amxc_string_take_buffer(&instance_path));

    SAH_TRACEZ_INFO(ME, "Build notification of type ObjectCreation for path = %s", GET_CHAR(path_var, NULL));
    retval = uspl_notify_new(usp_tx, &request);

    amxc_string_clean(&instance_path);
    amxc_var_clean(&request);
    return retval;
}

int uspi_subscription_build_object_creation(amxc_var_t* data,
                                            uspl_tx_t* usp_tx,
                                            uspi_sub_ref_t* sub_ref,
                                            bool send_resp) {
    int retval = -1;

    when_null(data, exit);
    when_null(usp_tx, exit);
    when_null(sub_ref, exit);
    when_null(sub_ref->sub, exit);
    when_null(sub_ref->sub->id, exit);

    retval = uspi_subscription_build_object_creation_common(data, usp_tx, sub_ref->sub->id, send_resp);

exit:
    return retval;
}

static int uspi_subscription_build_object_deletion_common(const amxc_var_t* const data,
                                                          uspl_tx_t* usp_tx,
                                                          const char* id,
                                                          bool send_resp) {
    int retval = -1;
    amxc_var_t request;
    amxc_var_t* obj_deletion = NULL;
    amxc_var_t* path = NULL;
    amxc_string_t instance_path;

    amxc_string_init(&instance_path, 0);
    amxc_var_init(&request);
    amxc_var_set_type(&request, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, &request, "notification_case", USP__NOTIFY__NOTIFICATION_OBJ_DELETION);
    amxc_var_add_key(cstring_t, &request, "subscription_id", id);
    amxc_var_add_key(bool, &request, "send_resp", send_resp);
    obj_deletion = amxc_var_add_key(amxc_htable_t, &request, "obj_deletion", NULL);

    amxc_string_setf(&instance_path, "%s%d.", GET_CHAR(data, "path"), GET_UINT32(data, "index"));
    path = amxc_var_add_new_key(obj_deletion, "path");
    amxc_var_push(cstring_t, path, amxc_string_take_buffer(&instance_path));

    SAH_TRACEZ_INFO(ME, "Build notification of type ObjectDeletion for path = %s",
                    amxc_var_constcast(cstring_t, path));
    retval = uspl_notify_new(usp_tx, &request);

    amxc_string_clean(&instance_path);
    amxc_var_clean(&request);
    return retval;
}

int uspi_subscription_build_object_deletion(amxc_var_t* data,
                                            uspl_tx_t* usp_tx,
                                            uspi_sub_ref_t* sub_ref,
                                            bool send_resp) {
    int retval = -1;

    when_null(data, exit);
    when_null(usp_tx, exit);
    when_null(sub_ref, exit);
    when_null(sub_ref->sub, exit);
    when_null(sub_ref->sub->id, exit);

    retval = uspi_subscription_build_object_deletion_common(data, usp_tx, sub_ref->sub->id, send_resp);

exit:
    return retval;
}

static int uspi_subscription_build_event_common(const amxc_var_t* const data,
                                                uspl_tx_t* usp_tx,
                                                const char* id,
                                                bool send_resp) {
    int retval = -1;
    amxc_var_t request;
    const char* path = GET_CHAR(data, "path");
    const char* event_name = GET_CHAR(data, "notification");
    amxc_var_t* params = GET_ARG(data, "data");
    amxc_var_t* event = NULL;

    amxc_var_init(&request);
    amxc_var_set_type(&request, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, &request, "notification_case", USP__NOTIFY__NOTIFICATION_EVENT);
    amxc_var_add_key(cstring_t, &request, "subscription_id", id);
    amxc_var_add_key(bool, &request, "send_resp", send_resp);
    event = amxc_var_add_key(amxc_htable_t, &request, "event", NULL);
    amxc_var_add_key(cstring_t, event, "path", path);
    amxc_var_add_key(cstring_t, event, "event_name", event_name);

    amxc_var_for_each(param, params) {
        const char* key = amxc_var_key(param);
        const char* value = NULL;

        // Add composite parameters in JSON string format
        if((amxc_var_type_of(param) == AMXC_VAR_ID_HTABLE) ||
           (amxc_var_type_of(param) == AMXC_VAR_ID_LIST)) {
            amxc_var_cast(param, AMXC_VAR_ID_JSON);
            value = amxc_var_constcast(jstring_t, param);
        } else {
            amxc_var_cast(param, AMXC_VAR_ID_CSTRING);
            value = amxc_var_constcast(cstring_t, param);
        }
        amxc_var_add_key(cstring_t, event, key, value);
    }

    SAH_TRACEZ_INFO(ME, "Build notification of type Event: %s%s", path, event_name);
    retval = uspl_notify_new(usp_tx, &request);

    amxc_var_clean(&request);
    return retval;
}

int uspi_subscription_build_event(amxc_var_t* data,
                                  uspl_tx_t* usp_tx,
                                  uspi_sub_ref_t* sub_ref,
                                  bool send_resp) {
    int retval = -1;

    when_null(data, exit);
    when_null(usp_tx, exit);
    when_null(sub_ref, exit);
    when_null(sub_ref->sub, exit);
    when_null(sub_ref->sub->id, exit);

    retval = uspi_subscription_build_event_common(data, usp_tx, sub_ref->sub->id, send_resp);

exit:
    return retval;
}

static int uspi_subscription_build_operation_complete_common(const amxc_var_t* const data,
                                                             uspl_tx_t* usp_tx,
                                                             const char* id,
                                                             bool send_resp) {
    int retval = -1;
    amxc_var_t request;
    amxc_var_t* oper_complete = NULL;
    const char* path = GET_CHAR(data, "path");
    uint32_t status = GETP_UINT32(data, "data.status");

    amxc_var_init(&request);
    amxc_var_set_type(&request, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, &request, "notification_case", USP__NOTIFY__NOTIFICATION_OPER_COMPLETE);
    amxc_var_add_key(cstring_t, &request, "subscription_id", id);
    amxc_var_add_key(bool, &request, "send_resp", send_resp);

    oper_complete = amxc_var_add_key(amxc_htable_t, &request, "oper_complete", NULL);
    amxc_var_add_key(cstring_t, oper_complete, "path", path);
    amxc_var_add_key(cstring_t, oper_complete, "cmd_name", GETP_CHAR(data, "data.cmd"));
    amxc_var_add_key(cstring_t, oper_complete, "cmd_key", GETP_CHAR(data, "data.cmd_key"));
    if(status == 0) {
        amxc_var_t* output_args = GETP_ARG(data, "data.output_args");
        amxc_var_add_key(uint32_t, oper_complete, "operation_case", USP__NOTIFY__OPERATION_COMPLETE__OPERATION_RESP_REQ_OUTPUT_ARGS);
        amxc_var_set_key(oper_complete, "output_args", output_args, AMXC_VAR_FLAG_COPY);
    } else {
        amxc_var_t* cmd_failure = NULL;
        uint32_t err_code = uspl_amxd_status_to_usp_error(status);

        amxc_var_add_key(uint32_t, oper_complete, "operation_case", USP__NOTIFY__OPERATION_COMPLETE__OPERATION_RESP_CMD_FAILURE);
        cmd_failure = amxc_var_add_key(amxc_htable_t, oper_complete, "cmd_failure", NULL);
        amxc_var_add_key(uint32_t, cmd_failure, "err_code", err_code);
        amxc_var_add_key(cstring_t, cmd_failure, "err_msg", uspl_error_code_to_str(err_code));
    }

    SAH_TRACEZ_INFO(ME, "Build notification of type OperationComplete for path = %s", path);
    retval = uspl_notify_new(usp_tx, &request);

    amxc_var_clean(&request);
    return retval;
}

int uspi_subscription_build_operation_complete(amxc_var_t* data,
                                               uspl_tx_t* usp_tx,
                                               uspi_sub_ref_t* sub_ref,
                                               bool send_resp) {
    int retval = -1;

    when_null(data, exit);
    when_null(usp_tx, exit);
    when_null(sub_ref, exit);
    when_null(sub_ref->sub, exit);
    when_null(sub_ref->sub->id, exit);

    retval = uspi_subscription_build_operation_complete_common(data, usp_tx, sub_ref->sub->id, send_resp);

exit:
    return retval;
}

int uspi_subscription_build_amx(amxc_var_t* data,
                                uspl_tx_t* usp_tx,
                                uspi_sub_ref_t* sub_ref,
                                bool send_resp) {
    int retval = -1;
    amxc_var_t request;
    uspi_sub_t* sub = sub_ref->sub;
    amxc_var_t* amx_notification = NULL;

    amxc_var_init(&request);
    amxc_var_set_type(&request, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, &request, "notification_case", USP__NOTIFY__NOTIFICATION_AMX_NOTIFICATION);
    amxc_var_add_key(cstring_t, &request, "subscription_id", sub->id);
    amxc_var_add_key(bool, &request, "send_resp", send_resp);
    amx_notification = amxc_var_add_key(amxc_htable_t, &request, "amx_notification", NULL);
    amxc_var_move(amx_notification, data);

    SAH_TRACEZ_INFO(ME, "Build notification of type AmxNotification for path = %s",
                    GET_CHAR(amx_notification, "path"));
    retval = uspl_notify_new(usp_tx, &request);

    amxc_var_clean(&request);
    return retval;
}

int uspi_subscription_build_notify(amxc_var_t* data,
                                   uspi_sub_ref_t* sub_ref,
                                   uspl_tx_t* usp_tx,
                                   bool send_resp) {
    int retval = -1;
    const char* notification = GET_CHAR(data, "notification");
    uspi_request_builder_t fn[] = {
        {"dm:object-changed", uspi_subscription_build_value_change},
        {"dm:instance-added", uspi_subscription_build_object_creation},
        {"dm:instance-removed", uspi_subscription_build_object_deletion},
        {"dm:operation-complete", uspi_subscription_build_operation_complete},
        {NULL, NULL},
    };

    when_null(data, exit);
    when_null(sub_ref, exit);
    when_null(usp_tx, exit);
    when_str_empty(notification, exit);

    if(strcmp(sub_ref->sub->notif_type, "AmxNotification") == 0) {
        retval = uspi_subscription_build_amx(data, usp_tx, sub_ref, send_resp);
        goto exit;
    }

    for(int i = 0; fn[i].notification_type != NULL; i++) {
        if(strcmp(fn[i].notification_type, notification) == 0) {
            retval = fn[i].fn(data, usp_tx, sub_ref, send_resp);
            goto exit;
        }
    }

    if(notification[strlen(notification) - 1] == '!') {
        retval = uspi_subscription_build_event(data, usp_tx, sub_ref, send_resp);
    }

exit:
    return retval;
}

int uspi_subscription_build_notify_v2(const amxc_var_t* const data,
                                      uspi_notify_type_t type,
                                      const char* id,
                                      uspl_tx_t* usp_tx,
                                      bool send_resp) {
    int retval = -1;

    when_null(data, exit);
    when_null(id, exit);
    when_null(usp_tx, exit);

    switch(type) {
    case notify_value_changed:
        retval = uspi_subscription_build_value_change_v2(data, usp_tx, id, send_resp);
        break;
    case notify_object_creation:
        retval = uspi_subscription_build_object_creation_common(data, usp_tx, id, send_resp);
        break;
    case notify_object_deletion:
        retval = uspi_subscription_build_object_deletion_common(data, usp_tx, id, send_resp);
        break;
    case notify_operation_complete:
        retval = uspi_subscription_build_operation_complete_common(data, usp_tx, id, send_resp);
        break;
    case notify_event:
        retval = uspi_subscription_build_event_common(data, usp_tx, id, send_resp);
        break;
    default:
        break;
    }

exit:
    return retval;
}
