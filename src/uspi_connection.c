/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>

#include <imtp/imtp_connection.h>

#include "uspi.h"
#include "uspi/uspi_connection.h"

static void uspi_buffer_reset(uspi_con_t* con) {
    when_null(con, exit);

    free(con->buffer);
    con->buffer = NULL;
    con->read_len = 0;

exit:
    return;
}

/**
   Returns:
    0 in case len bytes could be read,
    1 in case at least 1 byte, but fewer than len bytes could be read
    -1 in case no bytes could be read (broken connection)
 */
static int uspi_con_read_part(uspi_con_t* con, int len) {
    int retval = -1;
    int read_len = 0;

    read_len = amxb_read_raw(con->imtp_ctx,
                             con->buffer + con->read_len,
                             len);
    if(read_len <= 0) {
        SAH_TRACEZ_ERROR(ME, "Error reading %d bytes of IMTP message, read_len = %d",
                         len, read_len);
        uspi_buffer_reset(con);
        retval = -1;
        goto exit;
    }

    con->read_len += read_len;
    if(read_len < len) {
        retval = 1;
        goto exit;
    }

    retval = 0;
exit:
    return retval;
}

static int uspi_con_new(uspi_con_t** con, int flags) {
    int retval = -1;

    when_null(con, exit);

    *con = calloc(1, sizeof(uspi_con_t));
    when_null(*con, exit);

    (*con)->flags = flags;

    retval = 0;
exit:
    return retval;
}

static void uspi_con_delete(uspi_con_t** con) {
    when_null(con, exit);
    when_null(*con, exit);

    free((*con)->eid);
    amxc_llist_it_take(&(*con)->lit);
    if(!(*con)->read_done) {
        free((*con)->buffer);
    }
    amxb_free(&(*con)->imtp_ctx);

    free(*con);
    *con = NULL;

exit:
    return;
}

static void uspi_con_list_it_free(amxc_llist_it_t* it) {
    uspi_con_t* con = amxc_container_of(it, uspi_con_t, lit);
    uspi_con_delete(&con);
}

static int uspi_con_read_impl(uspi_con_t* con, int prefix_size) {
    int retval = -1;
    int length_size = sizeof(uint32_t);
    int header_len = prefix_size + length_size;
    uint32_t len_nbo = 0;
    uint32_t msg_len = 0;

    // Re-allocate buffer if previous read finished
    if((con->buffer == NULL) || con->read_done) {
        con->buffer = calloc(1, header_len);
        when_null(con->buffer, exit);
        con->read_len = 0;
        con->read_done = false;
    }

    if(con->read_len < header_len) {
        retval = uspi_con_read_part(con, header_len - con->read_len);
        when_failed(retval, exit);
    }

    memcpy(&len_nbo, con->buffer + prefix_size, length_size);
    msg_len = ntohl(len_nbo);

    // Only realloc memory once; not if this was already done previously
    if(con->read_len == header_len) {
        uint8_t* ptr = (uint8_t*) realloc(con->buffer, msg_len + header_len);
        if(ptr == NULL) {
            SAH_TRACEZ_ERROR(ME, "Unable to realloc %d bytes", msg_len + header_len);
            uspi_buffer_reset(con);
            retval = -1;
            goto exit;
        } else {
            con->buffer = ptr;
        }
    }

    retval = uspi_con_read_part(con, msg_len - con->read_len + header_len);

exit:
    return retval;
}

int uspi_con_connect(uspi_con_t** con, const char* uri) {
    int retval = -1;

    when_null(con, exit);
    when_str_empty(uri, exit);

    retval = uspi_con_new(con, USPI_CON_CONNECT);
    when_failed(retval, exit);

    retval = amxb_connect(&(*con)->imtp_ctx, uri);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to connect to uri %s with retval %d", uri, retval);
        uspi_con_delete(con);
        goto exit;
    }

exit:
    return retval;
}

void uspi_con_disconnect(uspi_con_t** con) {
    when_null(con, exit);
    when_null(*con, exit);

    if(((*con)->flags & USPI_CON_LISTEN) == USPI_CON_LISTEN) {
        amxc_llist_clean(&(*con)->accepted_cons, uspi_con_list_it_free);
    }

    uspi_con_delete(con);

exit:
    return;
}

int uspi_con_listen(uspi_con_t** con, const char* uri) {
    int retval = -1;

    when_null(con, exit);
    when_str_empty(uri, exit);

    retval = uspi_con_new(con, USPI_CON_LISTEN);
    when_failed(retval, exit);

    retval = amxb_listen(&(*con)->imtp_ctx, uri);
    if(retval != 0) {
        uspi_con_delete(con);
        SAH_TRACEZ_ERROR(ME, "Failed to listen on uri %s with retval %d", uri, retval);
        goto exit;
    }

    amxc_llist_init(&(*con)->accepted_cons);

exit:
    return retval;
}

int uspi_con_accept(uspi_con_t* con_listen, uspi_con_t** con_accepted) {
    int retval = -1;

    when_null(con_listen, exit);
    when_null(con_accepted, exit);

    retval = uspi_con_new(con_accepted, USPI_CON_ACCEPT);
    when_failed(retval, exit);

    retval = amxb_accept(con_listen->imtp_ctx, &(*con_accepted)->imtp_ctx);
    if(retval != 0) {
        uspi_con_delete(con_accepted);
        SAH_TRACEZ_ERROR(ME, "Failed to accept connection with retval %d", retval);
        goto exit;
    }

    amxc_llist_append(&con_listen->accepted_cons, &(*con_accepted)->lit);

exit:
    return retval;
}

int uspi_con_get_fd(uspi_con_t* con) {
    int retval = -1;

    when_null(con, exit);
    when_null(con->imtp_ctx, exit);

    retval = amxb_get_fd(con->imtp_ctx);
exit:
    return retval;
}

int uspi_con_read(uspi_con_t* con, imtp_tlv_t** tlv) {
    int retval = -1;

    when_null(con, exit);
    when_null(tlv, exit);

    retval = uspi_con_read_impl(con, 1);
    if(retval == 0) {
        con->read_done = true;
        retval = imtp_message_parse(tlv, con->buffer);
        if(retval < 0) {
            free(con->buffer);
            con->buffer = NULL;
            SAH_TRACEZ_ERROR(ME, "Unable to parse TLV message");
            goto exit;
        }
    }

exit:
    return retval;
}

int uspi_con_read_frame(uspi_con_t* con, imtp_frame_t** frame) {
    int retval = -1;
    int prefix_size = strlen(IMTP_SYNC_USP);

    when_null(con, exit);
    when_null(frame, exit);

    retval = uspi_con_read_impl(con, prefix_size);
    if(retval == 0) {
        con->read_done = true;
        retval = imtp_frame_parse(frame, con->buffer);
        if(retval < 0) {
            free(con->buffer);
            con->buffer = NULL;
            SAH_TRACEZ_ERROR(ME, "Unable to parse IMTP Frame message");
            goto exit;
        }
    }

exit:
    return retval;
}

int uspi_con_write(uspi_con_t* con, imtp_tlv_t* tlv) {
    int retval = -1;
    void* bytes = NULL;

    when_null(con, exit);
    when_null(tlv, exit);

    retval = imtp_message_to_bytes(tlv, &bytes);
    when_failed(retval, exit);

    retval = write(uspi_con_get_fd(con), bytes, tlv->length + 5);
    if(retval < 0) {
        SAH_TRACEZ_ERROR(ME, "Error sending TLV on fd: %d", uspi_con_get_fd(con));
        goto exit;
    }

    retval = 0;
exit:
    free(bytes);
    return retval;
}

int uspi_con_write_frame(uspi_con_t* con, imtp_frame_t* frame) {
    int retval = -1;
    void* bytes = NULL;
    uint32_t frame_header_len = strlen(IMTP_SYNC_USP) + sizeof(uint32_t);

    when_null(con, exit);
    when_null(frame, exit);

    retval = imtp_frame_to_bytes(frame, &bytes);
    when_failed(retval, exit);

    retval = write(uspi_con_get_fd(con), bytes, frame->length + frame_header_len);
    if(retval < 0) {
        SAH_TRACEZ_ERROR(ME, "Error sending TLV on fd: %d", uspi_con_get_fd(con));
        goto exit;
    }

    retval = 0;
exit:
    free(bytes);
    return retval;
}

int uspi_con_add_eid(uspi_con_t* con, const imtp_tlv_t* tlv_eid) {
    int retval = -1;
    amxc_var_t data;

    amxc_var_init(&data);

    when_null(con, exit);
    when_null(tlv_eid, exit);
    when_true(con->eid != NULL, exit);

    con->eid = (char*) calloc(1, tlv_eid->length + 1);
    when_null(con->eid, exit);
    strncpy(con->eid, (char*) tlv_eid->value + tlv_eid->offset, tlv_eid->length);

    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &data, "EndpointID", con->eid);
    amxc_var_add_key(uint32_t, &data, "fd", uspi_con_get_fd(con));
    amxp_sigmngr_emit_signal(NULL, USPI_SIG_EID, &data);

    retval = 0;
exit:
    amxc_var_clean(&data);
    return retval;
}

int uspi_con_send_eid(uspi_con_t* con, const char* eid) {
    int retval = -1;
    void* bytes = NULL;
    imtp_frame_t* frame = NULL;
    imtp_tlv_t* tlv_eid = NULL;

    when_null(con, exit);
    when_str_empty(eid, exit);

    imtp_frame_new(&frame);
    imtp_tlv_new(&tlv_eid, imtp_tlv_type_handshake, strlen(eid),
                 (char*) eid, 0, IMTP_TLV_COPY);
    imtp_frame_tlv_add(frame, tlv_eid);

    retval = uspi_con_write_frame(con, frame);

exit:
    free(bytes);
    imtp_frame_delete(&frame);
    return retval;
}

int uspi_con_flag_toggle(uspi_con_t* con, int flags) {
    int retval = -1;

    when_null(con, exit);

    con->flags ^= flags;
    retval = 0;
exit:
    return retval;
}

bool uspi_con_flag_is_set(uspi_con_t* con, int flag) {
    bool retval = false;

    when_null(con, exit);

    retval = (con->flags & flag) == flag;
exit:
    return retval;
}
