/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>

#include "uspi.h"
#include "uspi/uspi_utils.h"
#include "uspi/uspi_message.h"

typedef int (* usp_request_fn_t) (uspl_rx_t* usp_rx, amxc_var_t* result);
typedef int (* usp_resp_fn_t) (uspl_rx_t* usp_rx, amxc_llist_t* response);

typedef struct _uspi_msg_handler {
    uint32_t id;
    uspi_msg_type_t type;
    usp_request_fn_t extract_req_fn;
    usp_resp_fn_t extract_resp_fn;
} uspi_msg_handler_t;

static uspi_msg_handler_t fn[] = {
    { USP__HEADER__MSG_TYPE__ERROR, uspi_msg_type_error, NULL, NULL },
    { USP__HEADER__MSG_TYPE__GET, uspi_msg_type_request, uspl_get_extract, NULL },
    { USP__HEADER__MSG_TYPE__GET_RESP, uspi_msg_type_response, NULL, uspl_get_resp_extract},
    { USP__HEADER__MSG_TYPE__NOTIFY, uspi_msg_type_request, uspl_notify_extract, NULL},
    { USP__HEADER__MSG_TYPE__SET, uspi_msg_type_request, uspl_set_extract, NULL },
    { USP__HEADER__MSG_TYPE__SET_RESP, uspi_msg_type_response, NULL, uspl_set_resp_extract},
    { USP__HEADER__MSG_TYPE__OPERATE, uspi_msg_type_request, uspl_operate_extract, NULL},
    { USP__HEADER__MSG_TYPE__OPERATE_RESP, uspi_msg_type_response, NULL, uspl_operate_resp_extract},
    { USP__HEADER__MSG_TYPE__ADD, uspi_msg_type_request, uspl_add_extract, NULL},
    { USP__HEADER__MSG_TYPE__ADD_RESP, uspi_msg_type_response, NULL, uspl_add_resp_extract},
    { USP__HEADER__MSG_TYPE__DELETE, uspi_msg_type_request, uspl_delete_extract, NULL},
    { USP__HEADER__MSG_TYPE__DELETE_RESP, uspi_msg_type_response, NULL, uspl_delete_resp_extract},
    { USP__HEADER__MSG_TYPE__GET_SUPPORTED_DM, uspi_msg_type_request, uspl_get_supported_dm_extract, NULL},
    { USP__HEADER__MSG_TYPE__GET_SUPPORTED_DM_RESP, uspi_msg_type_response, NULL, uspl_get_supported_dm_resp_extract},
    { USP__HEADER__MSG_TYPE__GET_INSTANCES, uspi_msg_type_request, uspl_get_instances_extract, NULL},
    { USP__HEADER__MSG_TYPE__GET_INSTANCES_RESP, uspi_msg_type_response, NULL, uspl_get_instances_resp_extract},
    { USP__HEADER__MSG_TYPE__NOTIFY_RESP, uspi_msg_type_response, NULL, NULL},
    { USP__HEADER__MSG_TYPE__GET_SUPPORTED_PROTO, uspi_msg_type_request, NULL, NULL},
    { USP__HEADER__MSG_TYPE__GET_SUPPORTED_PROTO_RESP, uspi_msg_type_response, NULL, NULL},
    { USP__HEADER__MSG_TYPE__REGISTER, uspi_msg_type_request, uspl_register_extract, NULL},
    { USP__HEADER__MSG_TYPE__REGISTER_RESP, uspi_msg_type_response, NULL, uspl_register_resp_extract},
};

static uspi_msg_type_t uspi_message_get_outer_type(uint32_t inner_type) {
    uspi_msg_type_t retval = uspi_msg_type_max;
    if(inner_type < (sizeof(fn) / sizeof(fn[0]))) {
        retval = fn[inner_type].type;
    }
    return retval;
}

static int uspi_extract_helper(uspi_msg_t* msg) {
    int retval = -1;

    if(msg->inner_type == USP__HEADER__MSG_TYPE__ERROR) {
        SAH_TRACEZ_ERROR(ME, "Received error message from agent");
        retval = uspl_error_resp_extract(msg->usp_rx, msg->request);
    } else if(msg->inner_type < (sizeof(fn) / sizeof(fn[0]))) {
        if(fn[msg->inner_type].extract_req_fn != NULL) {
            retval = fn[msg->inner_type].extract_req_fn(msg->usp_rx, msg->request);
        } else if(fn[msg->inner_type].extract_resp_fn != NULL) {
            retval = fn[msg->inner_type].extract_resp_fn(msg->usp_rx, msg->resp_list);
        }
    }

    return retval;
}

int uspi_message_new(uspi_msg_t** msg,
                     uint32_t inner_type,
                     uspi_msg_type_t outer_type,
                     uspl_rx_t* usp_rx) {
    int retval = -1;
    amxc_var_t* request = NULL;
    amxc_llist_t* resp_list = NULL;

    when_null(msg, exit);
    when_null(usp_rx, exit);

    *msg = calloc(1, sizeof(uspi_msg_t));
    when_null(*msg, exit);

    if(outer_type == uspi_msg_type_request) {
        amxc_var_new(&request);
        (*msg)->request = request;
    } else if(outer_type == uspi_msg_type_response) {
        amxc_llist_new(&resp_list);
        (*msg)->resp_list = resp_list;
    }
    (*msg)->outer_type = outer_type;
    (*msg)->inner_type = inner_type;
    (*msg)->usp_rx = usp_rx;

    retval = 0;
exit:
    return retval;
}

void uspi_message_delete(uspi_msg_t** msg) {
    when_null(msg, exit);
    when_null(*msg, exit);

    uspl_rx_delete(&(*msg)->usp_rx);
    amxc_llist_delete(&(*msg)->resp_list, variant_list_it_free);
    amxc_var_delete(&(*msg)->request);
    free(*msg);
    *msg = NULL;

exit:
    return;
}

int uspi_message_extract(uspi_msg_t** msg, unsigned char* pbuf, int pbuf_len) {
    int retval = -1;
    uspl_rx_t* usp_rx = NULL;
    uint32_t inner_type = 0;
    uspi_msg_type_t outer_type = uspi_msg_type_error;

    when_null(msg, exit);
    when_null(pbuf, exit);

    usp_rx = uspl_msghandler_unpack_protobuf(pbuf, pbuf_len);
    when_null(usp_rx, exit);

    inner_type = uspl_msghandler_msg_type(usp_rx);
    outer_type = uspi_message_get_outer_type(inner_type);
    when_true(outer_type == uspi_msg_type_max, exit);

    retval = uspi_message_new(msg, inner_type, outer_type, usp_rx);
    when_failed(retval, exit);

    retval = uspi_extract_helper(*msg);
    when_failed(retval, exit);

    retval = 0;
exit:
    return retval;
}
