/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>

#include "uspi.h"
#include "uspi/uspi_discovery.h"

int uspi_discovery_add_service(amxb_bus_ctx_t* ctx, const char* eid) {
    int retval = -1;
    amxc_var_t values;
    amxc_var_t ret;

    amxc_var_init(&ret);
    amxc_var_init(&values);
    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);

    when_null(ctx, exit);
    when_str_empty(eid, exit);

    amxc_var_add_key(cstring_t, &values, "EndpointID", eid);

    retval = amxb_add(ctx, "Discovery.Service.", 0, NULL, &values, &ret, 5);

exit:
    amxc_var_clean(&values);
    amxc_var_clean(&ret);
    return retval;
}

int uspi_discovery_del_service(amxb_bus_ctx_t* ctx, const char* eid) {
    int retval = -1;
    amxc_var_t ret;
    amxc_string_t path;

    amxc_var_init(&ret);
    amxc_string_init(&path, 0);

    when_null(ctx, exit);
    when_str_empty(eid, exit);

    amxc_string_setf(&path, "Discovery.Service.[EndpointID == '%s'].", eid);

    retval = amxb_del(ctx, amxc_string_get(&path, 0), 0, NULL, &ret, 5);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to del Discovery.Service. instance with eid = %s", eid);
        goto exit;
    }

exit:
    amxc_string_clean(&path);
    amxc_var_clean(&ret);
    return retval;
}

int uspi_discovery_add_registration(amxb_bus_ctx_t* ctx, const char* eid, const char* path) {
    int retval = -1;
    amxc_var_t values;
    amxc_var_t ret;
    amxc_string_t object;

    amxc_string_init(&object, 0);
    amxc_var_init(&ret);
    amxc_var_init(&values);
    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);

    when_null(ctx, exit);
    when_str_empty(eid, exit);
    when_str_empty(path, exit);

    amxc_string_setf(&object, "Discovery.Service.[EndpointID == '%s'].Registration.", eid);

    amxc_var_add_key(cstring_t, &values, "Path", path);

    retval = amxb_add(ctx, amxc_string_get(&object, 0), 0, NULL, &values, &ret, 5);

exit:
    amxc_var_clean(&values);
    amxc_var_clean(&ret);
    amxc_string_clean(&object);
    return retval;
}

int uspi_discovery_del_registration(amxb_bus_ctx_t* ctx, const char* eid, const char* path) {
    int retval = -1;
    amxc_var_t ret;
    amxc_string_t object;

    amxc_string_init(&object, 0);
    amxc_var_init(&ret);

    when_null(ctx, exit);
    when_str_empty(eid, exit);
    when_str_empty(path, exit);

    amxc_string_setf(&object, "Discovery.Service.[EndpointID == '%s'].Registration.[Path == '%s'].",
                     eid, path);
    retval = amxb_del(ctx, amxc_string_get(&object, 0), 0, NULL, &ret, 5);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to add delete %s", amxc_string_get(&object, 0));
    }

exit:
    amxc_string_clean(&object);
    amxc_var_clean(&ret);
    return retval;
}
