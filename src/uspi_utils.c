/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <sys/types.h>
#include <regex.h>

#include <usp/uspl.h>

#include "uspi.h"
#include "uspi/uspi_utils.h"
#include "uspi/uspi_connection.h"

int uspi_utils_add_controller(amxb_bus_ctx_t* ctx,
                              const char* from_id,
                              amxc_var_t* params,
                              const char* param_name) {
    int retval = -1;
    amxc_string_t contr_path;
    amxc_var_t ret;
    amxc_var_t* contr = NULL;
    amxc_var_t* param_name_var = NULL;

    amxc_var_init(&ret);
    amxc_string_init(&contr_path, 0);

    when_null(ctx, exit);
    when_str_empty(from_id, exit);
    when_null(params, exit);
    when_str_empty(param_name, exit);

    amxc_string_setf(&contr_path, "LocalAgent.Controller.[EndpointID == '%s'].", from_id);

    retval = amxb_get(ctx, amxc_string_get(&contr_path, 0), 0, &ret, 5);
    contr = GETP_ARG(&ret, "0.0");

    if((retval != 0) || (contr == NULL)) {
        SAH_TRACEZ_ERROR(ME, "Failed to find controller with EID: %s", from_id);
        goto exit;
    }

    amxc_string_setf(&contr_path, "Device.%s", amxc_var_key(contr));
    amxc_string_remove_at(&contr_path, amxc_string_text_length(&contr_path) - 1, 1);
    param_name_var = amxc_var_add_new_key(params, param_name);
    amxc_var_push(cstring_t, param_name_var, amxc_string_take_buffer(&contr_path));

exit:
    amxc_string_clean(&contr_path);
    amxc_var_clean(&ret);
    return retval;
}

bool uspi_utils_matches(const char* regex_str, const char* path) {
    bool retval = false;
    regex_t regex;

    when_str_empty(regex_str, exit);
    when_str_empty(path, exit);

    regcomp(&regex, regex_str, REG_EXTENDED);
    if(regexec(&regex, path, 0, NULL, 0) == 0) {
        retval = true;
    } else {
        retval = false;
    }

    regfree(&regex);

exit:
    return retval;
}

amxc_string_t* uspi_utils_add_dot(const char* str_in) {
    size_t len = 0;
    amxc_string_t* dotted_path = NULL;

    when_str_empty(str_in, exit);

    amxc_string_new(&dotted_path, 0);
    amxc_string_setf(dotted_path, "%s", str_in);
    len = amxc_string_text_length(dotted_path);

    if(str_in[len - 1] != '.') {
        amxc_string_append(dotted_path, ".", 1);
    }

exit:
    return dotted_path;
}

amxc_var_t* uspi_utils_get_param_value(const char* obj_path, const char* param) {
    int retval = -1;
    amxb_bus_ctx_t* ctx = amxb_be_who_has(obj_path);
    amxc_var_t ret;
    amxc_string_t param_path;
    amxc_var_t* param_value = NULL;
    amxc_var_t* param_ret = NULL;

    amxc_var_new(&param_ret);
    amxc_var_init(&ret);
    amxc_string_init(&param_path, 0);

    when_str_empty(obj_path, exit);
    when_str_empty(param, exit);
    when_null(ctx, exit);

    amxc_string_setf(&param_path, "0.0.%s", param);

    when_str_empty(obj_path, exit);

    retval = amxb_get(ctx, obj_path, 0, &ret, 5);
    when_failed(retval, exit);

    param_value = GETP_ARG(&ret, amxc_string_get(&param_path, 0));
    when_null(param_value, exit);

    amxc_var_move(param_ret, param_value);

exit:
    amxc_var_clean(&ret);
    amxc_string_clean(&param_path);
    return param_ret;
}

uint32_t uspi_utils_get_con_type(uint32_t msg_type) {
    uint32_t con_type = USPI_CON_AGENT;

    switch(msg_type) {
    case USP__HEADER__MSG_TYPE__GET:
    case USP__HEADER__MSG_TYPE__SET:
    case USP__HEADER__MSG_TYPE__ADD:
    case USP__HEADER__MSG_TYPE__DELETE:
    case USP__HEADER__MSG_TYPE__GET_INSTANCES:
    case USP__HEADER__MSG_TYPE__GET_SUPPORTED_DM:
    case USP__HEADER__MSG_TYPE__OPERATE:
    case USP__HEADER__MSG_TYPE__NOTIFY_RESP:
    case USP__HEADER__MSG_TYPE__REGISTER_RESP:
    case USP__HEADER__MSG_TYPE__DEREGISTER_RESP:
    case USP__HEADER__MSG_TYPE__GET_SUPPORTED_PROTO:
        con_type = USPI_CON_CONTROLLER;
        break;
    default:
        break;
    }

    return con_type;
}

bool uspi_utils_check_sender(uint32_t msg_type, uint32_t con_type) {
    bool retval = false;
    uint32_t msg_con_type = uspi_utils_get_con_type(msg_type);

    if((msg_con_type & con_type) != 0) {
        retval = true;
    }

    return retval;
}
