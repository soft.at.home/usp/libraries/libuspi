/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__USPI_MESSAGE_H__)
#define __USPI_MESSAGE_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>

#include <usp/uspl.h>

/**
   @file
   @brief
   USP IMTP message API
 */

/**
   @ingroup lib_uspi
   @defgroup uspi_message USPI message
 */

/**
 * @brief
 * A USP message can be a request, response or error message
 */
typedef enum _uspi_msg_type {
    uspi_msg_type_request,              // USP Request
    uspi_msg_type_response,             // USP Response
    uspi_msg_type_error,                // USP Error
    uspi_msg_type_max
} uspi_msg_type_t;

/**
 * @brief
 * USP message handling helper struct
 *
 */
typedef struct _uspi_msg {
    uspi_msg_type_t outer_type;         // request, response or error
    uint32_t inner_type;                // more specific message type
    amxc_var_t* request;                // request variant
    amxc_llist_t* resp_list;            // response list
    uspl_rx_t* usp_rx;                  // received usp message
} uspi_msg_t;

/**
 * @brief
 * Creates a new uspi_msg_t struct.
 *
 * Depending on the outer_type parameter, it will allocate memory for a request variant or a
 * response list.
 *
 * @note
 * The outer_type will be request, response (or error). The inner type indicates a more specific
 * type such as get, set, add,...
 *
 * @note
 * Memory is allocated by this function, which must be freed by calling @ref uspi_message_delete
 *
 * @param msg pointer to a pointer for a new uspi_msg_t
 * @param inner_type specific message type
 * @param outer_type request, response or error
 * @param usp_rx the received USP message
 * @return 0 in case of success, any other value indicates an error
 */
int uspi_message_new(uspi_msg_t** msg,
                     uint32_t inner_type,
                     uspi_msg_type_t outer_type,
                     uspl_rx_t* usp_rx);

/**
 * @brief
 * Deletes the uspi_msg_t and frees up allocated memory.
 *
 * @param msg pointer to a pointer of an allocated uspi_msg_t
 */
void uspi_message_delete(uspi_msg_t** msg);

/**
 * @brief
 * Extracts the protobuf message into a uspi_msg_t struct
 *
 * The type of the message that is extracted is indicated by the 'type' field of the struct.
 * When it is a request message, the extracted request variant can be found in the 'request' field.
 * When it is a response message, the extracted response list can be found in the 'resp_list' field.
 * When it is an error message, the extracted error can also be found in the 'request' field.
 *
 * @note
 * Memory is allocated by this function, which must be freed by calling @ref uspi_message_delete
 *
 * @param msg pointer to a pointer for a new uspi_msg_t
 * @param pbuf the USP protobuf message
 * @param pbuf_len the length of the protobuf
 * @return 0 in case of success, any other value indicates an error
 */
int uspi_message_extract(uspi_msg_t** msg, unsigned char* pbuf, int pbuf_len);

#ifdef __cplusplus
}
#endif

#endif // __USPI_MESSAGE_H__
