/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__USPI_SUBSCRIPTION_H__)
#define __USPI_SUBSCRIPTION_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_object.h>
#include <amxb/amxb.h>
#include <usp/uspl.h>

/**
   @file
   @brief
   USP IMTP subscription API
 */

/**
   @ingroup lib_uspi
   @defgroup uspi_subscription USPI subscription
 */

/**
   @ingroup uspi_subscription
   @brief
   Struct to keep track of USP subscriptions

   An instance of this struct will be created for each instance in the LocalAgent.Subscription
   data model.

   Most of the fields are explained below, but the sub_ref_list needs some extra explanation.
   Every LocalAgent.Subscription instance contains a ReferenceList which is a list of paths. For
   subscriptions of type ValueChange, it is possible that the Controller is only interested in one
   or a few parameters of a certain object. The ReferenceList could for example be:

   ```
   ReferenceList = "Device.IP.Interface.*.Enable,Device.IP.Interface.*.Status"
   ```

   In this case there should only be notifications for the Enable and Status parameters. In case
   several other values change as well, the controller is not interested in them and no notification
   should be built for them.

   To summarize:
   - When a LocalAgent.Subscription. instance is added, a uspi_sub_t struct should be created with
     @ref uspi_sub_new.
   - For each element in the ReferenceList, a uspi_sub_ref_t struct should be created with
     @ref uspi_sub_ref_new.
   - For each element in the ReferenceList, the uspi_sub_ref_t struct should be provided as private
     data to the amxb_subscribe function
   - In the event callback function, the uspi_sub_t struct can be retrieved from the uspi_sub_ref_t
     struct. In case of a 'dm:object-changed' event, the ref_path field of the uspi_sub_ref_t struct
     can be used to filter out the params that the controller is not interested in. For other types
     of events, the struct is not used (for now).
 */
typedef struct _uspi_sub {
    amxc_llist_it_t lit;               // Linked list iterator to find struct in list of subs
    char* instance_path;               // Path to the corresponding LocalAgent.Subscription.{i}. instance
    char* contr_path;                  // Path to the LocalAgent.Controller.{i}. recipient
    char* id;                          // Subscription ID
    char* notif_type;                  // NotifType
    amxc_llist_t sub_ref_list;         // List of uspi_sub_ref_t structs. There will be one for each entry in the ReferenceList.
    amxc_llist_t retry_list;           // List of uspi_sub_retry_t structs to keep track of notifications that must be retried.
    amxp_timer_t* periodic_timer;      // Timer for periodic subscriptions
} uspi_sub_t;

/**
   @ingroup uspi_subscription
   @brief
   Struct to keep track of the reference path that was used for the current subscription.

   There will be one of these structs for each element in the ReferenceList.

   This struct should be provided as the private data to amxb_subscribe. It can be used in the
   ValueChange notification callback function to extract the parameter that was subscribed to.
   In case the bus event contained several changed parameters, the redundant ones can be filtered
   out.
 */
typedef struct _uspi_sub_ref {
    amxc_llist_it_t lit;               // Linked list iterator to find struct in sub_ref_list of uspi_sub_t
    uspi_sub_t* sub;                   // Pointer to the encompassing uspi_sub_t struct
    char* ref_path;                    // Reference path for a single element in the ReferenceList
} uspi_sub_ref_t;

/**
   @ingroup uspi_subscription
   @brief
   Struct to keep track of notifications that need to be retried. There can be multiple for the
   same subscription instance.
 */
typedef struct _uspi_sub_retry {
    amxc_llist_it_t lit;                // Linked list iterator to find struct in list of retries
    uspl_tx_t* usp_tx;                  // Pointer to struct containing protobuf to retransmit
    amxp_timer_t* retry_timer;          // Timer to retry notification
    amxp_timer_t* expiration_timer;     // Timer to let notification expire
    uint32_t retry_time;                // Time until next notification
    uint32_t retry_attempts;            // Counter to keep track of how many times the notification has been retried
} uspi_sub_retry_t;

/**
   @ingroup uspi_subscription
   @brief
   Enumeration to track the different notification types.
 */
typedef enum _uspi_notify_type {
    notify_value_changed,
    notify_object_creation,
    notify_object_deletion,
    notify_operation_complete,
    notify_event
} uspi_notify_type_t;

/**
   @ingroup uspi_subscription
   @brief
   Creates a new uspi_sub_retry_t struct and adds it to the retry_list of the provided uspi_sub_t
   struct. Also starts a timer for retrying the notification. When the timer expires, the
   callback function that is provided will be called to try to retransmit the notification.

   The timer is calculated based on the LocalAgent.Controller.{i}.USPNotifRetryIntervalMultiplier
   and LocalAgent.Controller.{i}.USPNotifRetryMinimumWaitInterval parameters that are set for the
   controller that wants to receive the notification.

   @note
   This function allocates memory on the heap that must be freed with @ref uspi_sub_retry_delete

   @param sub subscription struct with information related to the subscribed controller
   @param usp_tx struct containing the USP notification that must be retried
   @param timer_cb function that must be called when the retry timer expires
   @param bus_ctx bus context used to retrieve the Controller parameters from the data model
 */
void uspi_sub_retry_new(uspi_sub_t* sub,
                        uspl_tx_t* usp_tx,
                        amxp_timer_cb_t timer_cb,
                        amxb_bus_ctx_t* bus_ctx);

/**
   @ingroup uspi_subscription
   @brief
   Deletes an instance of a uspi_sub_retry_t struct that was previously allocated with
   @ref uspi_sub_retry_new.

   @note
   This function should be called when a USP NotifyResponse is received by the USP agent. It is
   automatically called when @ref uspi_sub_delete is called, so it is normally not needed to call
   this function explicitly when the subscription is removed.

   @param sub_retry allocated uspi_sub_retry_t struct
 */
void uspi_sub_retry_delete(uspi_sub_retry_t** sub_retry);

/**
   @ingroup uspi_subscription
   @brief
   Creates a new uspi_sub_t struct.

   @note
   This function allocates memory on the heap for the uspi_sub_t struct, which must be freed by
   calling @ref uspi_sub_delete

   @param sub pointer to a pointer for a new uspi_sub_t struct
   @param inst_path LocalAgent.Subscription.{i}. data model path
   @param contr_path LocalAgent.Controller.{i}. data model path
   @param id LocalAgent.Subscription.{i}.ID
   @param notif_type LocalAgent.Subscription.{i}.NotifType
   @return 0 in case of success, -1 in case of error
 */
int uspi_sub_new(uspi_sub_t** sub,
                 const char* inst_path,
                 const char* contr_path,
                 const char* id,
                 const char* notif_type);

/**
   @ingroup uspi_subscription
   @brief
   Deletes the instances of the uspi_sub_t struct that was previously allocated with
   @ref uspi_sub_new

   @param sub allocated uspi_sub_t struct
 */
void uspi_sub_delete(uspi_sub_t** sub);

/**
   @ingroup uspi_subscription
   @brief
   Creates a new uspi_sub_ref_t struct and adds it to the sub_ref_list of the provided uspi_sub_t
   struct.

   @note
   This function allocates data for the uspi_sub_ref_t struct and links the struct with the
   uspi_sub_t struct. The allocated data will be freed when deleting the encompassing uspi_sub_t
   struct with @ref uspi_sub_delete.

   @param sub struct for the encompassing subscription
   @param ref_path element of the ReferenceList
   @return pointer to a uspi_sub_ref_t struct that should be provided as private data to
           amxb_subscribe, or NULL in case of error
 */
uspi_sub_ref_t* uspi_sub_ref_new(uspi_sub_t* sub, const char* ref_path);

/**
   @ingroup uspi_subscription
   @brief
   Finds a uspi_sub_ref_t struct in the sub_ref_list of the uspi_sub_t struct based on the provided
   reference path.

   @param sub struct for the subscription instance
   @param ref_path element of the ReferenceList
   @return pointer to a uspi_sub_ref_t struct if found, NULL otherwise
 */
uspi_sub_ref_t* uspi_sub_ref_find(uspi_sub_t* sub, const char* ref_path);

/**
   @ingroup uspi_subscription
   @brief
   Builds the needed filter expression to pass to amxb_subscribe based on the Path Name and
   notification type.

   It is able to build expressions for subscriptions of type ValueChange, ObjectCreation,
   ObjectDeletion and Event. Subscriptions of type OperationComplete should not be added with
   amxb_subscribe, hence it is not possible to build an expression for them.

   @param path Path Name for the subscription. Can be a Search Path.
   @param notif_type notification type of the subscription. Can be ValueChange, ObjectCreation,
                     ObjectDeletion or Event.
   @return expression string if the expression was built successfully, NULL otherwise
 */
char* uspi_subscription_build_expression(amxd_path_t* path,
                                         const char* notif_type);

/**
   @ingroup uspi_subscription
   @brief
   Builds a USP Notify message for a ValueChange notification based on the ambiorix event data.

   WARNING:
   This function modifies the provided data variant. If multiple controllers are subscribed to the
   same path, ambiorix will provide a pointer to the same variant for each callback function. It is
   important that a copy of the ambiorix event data variant is passed to this function to avoid
   issues with handling subscriptions for multiple controllers.

   @param data a copy of the ambiorix event data variant that is obtained when the amxb_subscribe
               callback function is called
   @param sub_ref subscription struct for the reference path of the current subscription instance
   @param usp_tx pointer to a uspl_tx_t struct that was allocated with uspl_tx_new
   @param send_resp indicates whether the Controller should send a response to the notification.
                    Only needed when NotifRetry is set to true for the subscription
   @return -1 in case of error or when no further events can be built based on the data variant,
            0 when a Notify message has successfully been built,
            1 when a Notify message was built, but the data variant might contain more information
            to build extra notifications
 */
int uspi_subscription_build_value_change(amxc_var_t* data,
                                         uspl_tx_t* usp_tx,
                                         uspi_sub_ref_t* sub_ref,
                                         bool send_resp);
/**
   @ingroup uspi_subscription
   @brief
   Builds a USP Notify message for an ObjectCreation notification based on the ambiorix event data.

   @param data ambiorix event data variant that is obtained when the amxb_subscribe callback
               function is called
   @param usp_tx pointer to a uspl_tx_t struct that was allocated with uspl_tx_new
   @param sub_ref subscription struct for the reference path of the current subscription instance
   @param send_resp indicates whether the Controller should send a response to the notification.
                    Only needed when NotifRetry is set to true for the subscription
   @return 0 when a Notify message was built, -1 otherwise
 */
int uspi_subscription_build_object_creation(amxc_var_t* data,
                                            uspl_tx_t* usp_tx,
                                            uspi_sub_ref_t* sub_ref,
                                            bool send_resp);
/**
   @ingroup uspi_subscription
   @brief
   Builds a USP Notify message for an ObjectDeletion notification based on the ambiorix event data.

   @param data ambiorix event data variant that is obtained when the amxb_subscribe callback
               function is called
   @param usp_tx pointer to a uspl_tx_t struct that was allocated with uspl_tx_new
   @param sub_ref subscription struct for the reference path of the current subscription instance
   @param send_resp indicates whether the Controller should send a response to the notification.
                    Only needed when NotifRetry is set to true for the subscription
   @return 0 when a Notify message was built, -1 otherwise
 */
int uspi_subscription_build_object_deletion(amxc_var_t* data,
                                            uspl_tx_t* usp_tx,
                                            uspi_sub_ref_t* sub_ref,
                                            bool send_resp);

/**
   @ingroup uspi_subscription
   @brief
   Builds a USP Notify message for an Event notification based on the ambiorix event data.

   @param data ambiorix event data variant that is obtained when the amxb_subscribe callback
               function is called
   @param usp_tx pointer to a uspl_tx_t struct that was allocated with uspl_tx_new
   @param sub_ref subscription struct for the reference path of the current subscription instance
   @param send_resp indicates whether the Controller should send a response to the notification.
                    Only needed when NotifRetry is set to true for the subscription
   @return 0 when a Notify message was built, -1 otherwise
 */
int uspi_subscription_build_event(amxc_var_t* data,
                                  uspl_tx_t* usp_tx,
                                  uspi_sub_ref_t* sub_ref,
                                  bool send_resp);

/**
   @ingroup uspi_subscription
   @brief
   Builds a USP Notify message for an operation-compolete notification based on the ambiorix event data.

   @param data ambiorix event data variant that is obtained when the amxb_subscribe callback
               function is called
   @param usp_tx pointer to a uspl_tx_t struct that was allocated with uspl_tx_new
   @param sub_ref subscription struct for the reference path of the current subscription instance
   @param send_resp indicates whether the Controller should send a response to the notification.
                    Only needed when NotifRetry is set to true for the subscription
   @return 0 when a Notify message was built, -1 otherwise
 */
int uspi_subscription_build_operation_complete(amxc_var_t* data,
                                               uspl_tx_t* usp_tx,
                                               uspi_sub_ref_t* sub_ref,
                                               bool send_resp);

/**
   @ingroup uspi_subscription
   @brief
   Builds a USP Notify message for a notification of type AmxNotification based on the ambiorix
   event data.

   WARNING:
   This function modifies the provided data variant. If multiple controllers are subscribed to the
   same path, ambiorix will provide a pointer to the same variant for each callback function. It is
   important that a copy of the ambiorix event data variant is passed to this function to avoid
   issues with handling subscriptions for multiple controllers.

   @param data ambiorix event data variant that is obtained when the amxb_subscribe callback
               function is called
   @param usp_tx pointer to a uspl_tx_t struct that was allocated with uspl_tx_new
   @param sub_ref subscription struct for the reference path of the current subscription instance
   @param send_resp indicates whether the Controller should send a response to the notification.
                    Only needed when NotifRetry is set to true for the subscription
   @return 0 when a Notify message was built, -1 otherwise
 */
int uspi_subscription_build_amx(amxc_var_t* data,
                                uspl_tx_t* usp_tx,
                                uspi_sub_ref_t* sub_ref,
                                bool send_resp);

/**
   @ingroup uspi_subscription
   @brief
   Builds a USP protobuf message of type Notify Request based on the ambiorix event data.

   @note
   Returns 1 when the data variant might contain more information to build extra notifications.
   This can happen when there is a subscription of type ValueChange and multiple parameters change
   in the same transaction.

   WARNING:
   This function modifies the provided data variant in case of a ValueChange notification. If
   multiple controllers are subscribed to the same path, ambiorix will provide a pointer to the same
   variant for each callback function. It is important that a copy of the ambiorix event data
   variant is passed to this function to avoid issues with handling subscriptions for multiple
   controllers.

   @param data ambiorix event data variant that is obtained when the amxb_subscribe callback
               function is called
   @param sub_ref subscription struct for the reference path of the current subscription instance
   @param usp_tx pointer to a uspl_tx_t struct that was allocated with uspl_tx_new
   @param send_resp indicates whether the Controller should send a response to the notification.
                    Only needed when NotifRetry is set to true for the subscription
   @return -1 in case of error or when no further events can be built based on the data variant,
            0 when a Notify message has successfully been built,
            1 when a Notify message was built, but the data variant might contain more information
            to build extra notifications
 */
int uspi_subscription_build_notify(amxc_var_t* data,
                                   uspi_sub_ref_t* sub_ref,
                                   uspl_tx_t* usp_tx,
                                   bool send_resp);

/**
   @ingroup uspi_subscription
   @brief
   Builds a USP protobuf message of type Notify Request based on the ambiorix event data.

   This function behaves similar to @ref uspi_subscription_build_notify with the following
   differences:
   - The input arguments are slightly different, you don't need to provide a uspi_sub_ref_t struct.
     In the original function this struct was mainly used to get the ID, which is now passed as a
     separate argument. It was also used to handle ValueChange notifications where it would
     maintain a list of parameters that correspond to the subscription, which is needed because
     an ambiorix dm:object-changed event can contain changes for a subscribed parameter and some
     extra parameters. A notification only needs to be built for the subscribed parameter(s).
     Because the struct is no longer provided to the function, this tracking needs to happen
     outside of this function and a custom data variant containing the information for a single
     parameter change need to be provided to this function. The provided variant should have the
     following format:
     ```
      {
          path = <object-path>,
          param_name = <param-name>,
          from = <old-value>,
          to = <new-value>
      }
     ```
   - The data variant is now a constant variant that will not be modified by the function. The
     function will no longer return 1 in case the data variant contains more information to build
     extra notifications, because this no longer applies (see above changes for ValueChange
     notifications). The only reason to call this function multiple times for the same data variant
     is if the same notification needs to be sent to a different controller (different uspl_tx_t
     struct), or if there are multiple subscriptions to the same data model with different
     subscription ids.

   @param data ambiorix event data variant from the event callback function or the custom data
               variant as described above in case of a dm:object-changed event
   @param type notification type that determines which type of notification must be built.
               One of @ref uspi_notify_type_t.
   @param id subscription ID associated with the notification
   @param usp_tx pointer to a uspl_tx_t struct that was allocated with uspl_tx_new
   @param send_resp indicates whether the Controller should send a response to the notification.
                    Only needed when NotifRetry is set to true for the subscription

   @return 0 in case of success, -1 in case of error.
 */
int uspi_subscription_build_notify_v2(const amxc_var_t* const data,
                                      uspi_notify_type_t type,
                                      const char* id,
                                      uspl_tx_t* usp_tx,
                                      bool send_resp);

#ifdef __cplusplus
}
#endif

#endif // __USPI_SUBSCRIPTION_H__
