/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__USPI_CONNECTION_H__)
#define __USPI_CONNECTION_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_types.h>
#include <amxb/amxb.h>
#include <imtp/imtp_frame.h>

/**
   @file
   @brief
   USP IMTP connection API
 */

/**
   @ingroup lib_uspi
   @defgroup uspi_connection USPI connection
 */

#define USPI_CON_LISTEN            0b00000001    // When the connection is used for a listen socket
#define USPI_CON_CONNECT           0b00000010    // When the connection is used to connect to a listen socket
#define USPI_CON_ACCEPT            0b00000100    // For accepted sockets
#define USPI_CON_AGENT             0b00001000    // Connection is used by an agent
#define USPI_CON_CONTROLLER        0b00010000    // Connection is used by a controller

typedef struct _uspi_con {
    amxc_llist_it_t lit;            // to track the struct in a linked list (used internally)
    amxb_bus_ctx_t* imtp_ctx;       // ptr to the bus context used for communicating
    amxc_rbuffer_t* write_buf;      // buffer to save (partial) write IMTP message
    uint8_t* buffer;                // buffer to save (partial) read IMTP message
    ssize_t read_len;               // keeps track of the number of bytes read for the current IMTP message
    bool read_done;                 // boolean to check if we should clear the buffer before reading again
    char* eid;                      // EndpointID of the USP entity on the other end of this connection
    int flags;                      // Bitwise OR combination of the USPI_CON flags listed above
    amxc_llist_t accepted_cons;     // List of accepted connections. Only used when type is listen
    void* priv;                     // possible private data that needs to be tracked. Can be filled in by the user
} uspi_con_t;

/**
   @ingroup uspi_connection
   @brief
   Connect to the provided uri using amxb_connect.

   @note
   There must be an ambiorix back-end for the provided uri.

   @note
   Memory will be allocated for the uspi_con_t struct and this must be freed by calling
   @ref uspi_con_disconnect

   @param con pointer to a pointer for a new connect struct
   @param uri the connection uri, compliant with RFC 3986
   @return 0 in case of success, any other value indicates an error
 */
int uspi_con_connect(uspi_con_t** con, const char* uri);

/**
   @ingroup uspi_connection
   @brief
   Closes the previously established connection.

   Frees memory allocated for the struct and sets the pointer for the struct back to NULL.

   @note
   In case of a listen socket, it will also close all accepted connections and free memory
   allocated for these connections.

   @param con pointer to a pointer for the uspi_con_t struct that was previously created
 */
void uspi_con_disconnect(uspi_con_t** con);

/**
   @ingroup uspi_connection
   @brief
   Listen for incoming connections on the provided uri using amxb_listen.

   @note
   There must be an ambiorix back-end for the provided uri.

   @note
   Memory will be allocated for the uspi_con_t struct and this must be freed by calling
   @ref uspi_con_disconnect

   @param con pointer to a pointer for a new connect struct
   @param uri the listen uri, compliant with RFC 3986
   @return 0 in case of success, any other value indicates an error
 */
int uspi_con_listen(uspi_con_t** con, const char* uri);

/**
   @ingroup uspi_connection
   @brief
   Accepts an incoming connection using amxb_accept.

   @note
   Memory for the accepted connection can be freed by calling @ref uspi_con_disconnect on the
   listen connection.

   @param con_listen connection for the listen socket
   @param con_accepted pointer to a new struct for the accepted connection
   @return 0 in case of success, any other value indicates an error
 */
int uspi_con_accept(uspi_con_t* con_listen, uspi_con_t** con_accepted);

/**
   @ingroup uspi_connection
   @brief
   Gets the file descriptor for the connection

   @param con the connection struct
   @return a file descriptor larger than 0 in case of success, -1 in case of error
 */
int uspi_con_get_fd(uspi_con_t* con);

/**
   @ingroup uspi_connection
   @brief
   Reads an IMTP message from the connection, which could consist out of multiple TLVs. The
   provided tlv argument will be the header TLV.

   @note
   Memory will be allocated for the tlv struct, which must be freed with imtp_tlv_delete

   @param con the connection to read from
   @param tlv pointer to a pointer for a tlv that will be allocated
   @return
    0 in case a full IMTP could be read,
    1 in case a partial IMTP message could be read
    -1 in case no bytes could be read (broken connection or invalid input)
 */
int uspi_con_read(uspi_con_t* con, imtp_tlv_t** tlv);

/**
   @ingroup uspi_connection
   @brief
   Receives an IMTP frame over an IMTP connection.

   @note
   A message is received over the IMTP connection and parsed into a imtp_frame_t structure.
   The imtp_frame_t is created on the heap using imtp_frame_new and needs to be destroyed using
   imtp_frame_delete. Any TLVs that are part of the IMTP message will also be destroyed.

   @note
   It is possible that the entire IMTP message is too long to be read at once. If this happens,
   the function will return with value 1 and store the data that was already read internally in the
   passed uspi_con_t struct. The function must be called again when more data is ready to be
   received. When the function returns with value 0, the message was correctly received and can be
   found in the imtp_frame_t struct.

   @param con pointer to a imtp_connection_t.
   @param frame pointer to a pointer to an imtp_frame_t struct that will be created.
   @return 0 if the message is successfully received from the connection,
           -1 if an error occured,
           1 if the message could not be read entirely with a single read
 */
int uspi_con_read_frame(uspi_con_t* con, imtp_frame_t** frame);

/**
   @ingroup uspi_connection
   @brief
   Write an IMTP message on the provided connection

   @param con the connection to write on
   @param tlv pointer to a tlv message to send
   @return 0 in case of success, any other value indicates an error
 */
int uspi_con_write(uspi_con_t* con, imtp_tlv_t* tlv);

/**
   @ingroup uspi_connection
   @brief
   Sends a IMTP frame over an IMTP connection.

   @note
   Given a valid IMTP connection and a valid IMTP frame, the message is sent over the configured
   connection.

   @param con pointer to a valid imtp_connection_t structure.
   @param frame pointer to a valid imtp_frame_t structure.
   @return 0 if the message is successfully sent over the connection, -1 otherwise.
 */
int uspi_con_write_frame(uspi_con_t* con, imtp_frame_t* frame);

/**
   @ingroup uspi_connection
   @brief
   Adds an EndpointID to the connection struct and emits the signal
   "usp:eid-added" with the following hash table as data variant:
   {
       EndpointID = <the provided eid>
       fd = <the file descriptor for this con>
   }
   This signal is for example used by the USP back-end to updated the
   internally managed EndpointID for the bus connection as well.

   @note
   Will fail if the con already has a non NULL eid field

   @param con the connection struct to extend
   @param tlv_eid tlv containing an EndpointID
   @return 0 in case of success, any other value indicates an error
 */
int uspi_con_add_eid(uspi_con_t* con, const imtp_tlv_t* tlv_eid);

/**
   @ingroup uspi_connection
   @brief
   Sends the EndpointID over the connection

   @param con the connection struct used to communicate
   @param eid the EndpointID to send
   @return 0 in case of success, any other value indicates an error
 */
int uspi_con_send_eid(uspi_con_t* con, const char* eid);

/**
   @brief
   Toggles a flags for the connection.

   Useful to mark the connection as an agent connection when USPI_CON_AGENT is provided or mark it
   as a controller when USPI_CON_CONTROLLER is provided.

   @param con the connection to toggle the flag
   @param flags the flag
   @return 0 if the flag was toggled, -1 when a NULL pointer was provided for the con argument
 */
int uspi_con_flag_toggle(uspi_con_t* con, int flags);

/**
   @brief
   Checks the connection to see if a flag is set

   @param con the connection to check
   @param flag the flag to check
   @return true if the flag is set, false otherwise
 */
bool uspi_con_flag_is_set(uspi_con_t* con, int flag);

#ifdef __cplusplus
}
#endif

#endif // __USPI_CONNECTION_H__
