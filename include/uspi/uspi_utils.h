/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__USPI_UTILS_H__)
#define __USPI_UTILS_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_object.h>
#include <amxb/amxb.h>

/**
   @file
   @brief
   USP IMTP utility API
 */

/**
   @ingroup lib_uspi
   @defgroup uspi_utils USPI utility functions
 */

/**
   @ingroup uspi_utils
   @brief
   Adds the USP Controller data model path to the parameters of an add request

   Certain TR-181 multi-instance objects contain a parameter which is a reference to the
   Device.LocalAgent.Controller.{i}. instance that added the instance. This function will
   add the correct instance path to the parameters of the add request by checking the EndpointID
   of the Controller that made the request.

   Examples of parameters where this function is helpful are
    - Device.LocalAgent.Subscription.{i}.Recipient
    - Device.BulkData.Profile.{i}.Controller

   @param ctx bus context used to find the Device.LocalAgent data model
   @param from_id EndpointID of the Controller that made the add request
   @param params variant of type hash table with the rest of the parameters for the add request
   @param param_name name of the Controller reference parameter.
   @return 0 when the Controller path was correclty added to the parameters, any other value
           indicates an error
 */
int uspi_utils_add_controller(amxb_bus_ctx_t* ctx,
                              const char* from_id,
                              amxc_var_t* params,
                              const char* param_name);

/**
   @ingroup uspi_utils
   @brief
   Checks if a path matches a regex string

   @param regex_str string containing a regular expression
   @param path string to check whether it matches the regular expression
   @return true if path matches the regex string, false otherwise
 */
bool uspi_utils_matches(const char* regex_str, const char* path);

/**
   @ingroup uspi_utils
   @brief
   Adds a dot to a string if it doesn't already end in a dot

   @note
   This function allocates memory that must be freed with amxc_string_delete

   @param str_in input string
   @return dot terminated string in case off success, NULL in case of error
 */
amxc_string_t* uspi_utils_add_dot(const char* str_in);

/**
   @ingroup uspi_utils
   @brief
   Gets a parameter value from an object.

   @param obj_path the object to retrieve the parameter from
   @param param the parameter name
   @return variant containing the requested parameter or NULL in case of an error
 */
amxc_var_t* uspi_utils_get_param_value(const char* obj_path, const char* param);

/**
   @ingroup uspi_utils
   @brief
   Returns the connection type for a USP message. It can either be an agent or controller message
   or both.

   @param msg_type a USP__HEADER__MSG_TYPE
   @return USPI_CON_CONTROLLER if the message is a controller message and USPI_CON_AGENT otherwise
 */
uint32_t uspi_utils_get_con_type(uint32_t msg_type);

/**
   @ingroup uspi_utils
   @brief
   Checks whether the sender of agent messages is actually an agent and whether the sender of
   controller messages is actually a controller.

   Each uspi connection has a type USPI_CON_AGENT or USPI_CON_CONTROLLER which determines the
   logical role i.e. agent or controller of the endpoint on the other side. Agents should only send
   agent messages and controller should only send controller messages. This function will return
   true if the connection type matches the message type

   @param msg_type the type of the received USP message
   @param con_type the connection type
   @return true if an agent sent an agent message or a controller sent a controller message,
           false otherwise
 */
bool uspi_utils_check_sender(uint32_t msg_type, uint32_t con_type);

#ifdef __cplusplus
}
#endif

#endif // __USPI_UTILS_H__
